<?php


class ItemListModel extends CI_Model{
    
    public function __construct() {
      parent::__construct(); 
      $this->load->database();
      $this->load->helper('url', 'form');
      $this->load->library('session');
      $this->load->library('cart');
    }
   
    public function getItemList($limit, $start){
        $this->db->select('product_details.*,stock.amount,price.*');
        $this->db->from('product_details');
        $this->db->join('stock', 'stock.idproduct = product_details.id', 'left');
        $this->db->join('price', 'price.idproduct = product_details.id', 'left');
        if(!empty($this->input->post("search"))){
            $this->db->like('product_details.product_name', $this->input->post("search"));
            $this->db->or_like('product_details.product_number', $this->input->post("search")); 
        }
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result();

    }
    public function getItemListPost(){
        $query = $this->getItemList($this->input->post("limit"), $this->input->post("start")); //ez a bajos!
        $paginationHtml = "";
        $paginationHtml .= '
        <div>
           <table class="table table-bordered">
            <thead>
            <tr>
             <th>Terméknév</th>
             <th>Cikkszám</th>
             <th>Ár</th>
             <th>Mennyiség</th>
             <th>Rövid leírás</th>
             <th width="220px">Művelet</th>
            </tr>
            </thead><tbody>
        ';
        foreach($query as $row){
        $price = 0;
        $date_now = date("Y-m-d"); 
        if ($date_now >= $row->date_from && $date_now <= $row->date_to) {
              $price = $row->future_price;
        }else{
              $price = $row->vatprice;
        }
        $paginationHtml.='<tr>';
	$paginationHtml.='<td>'.$row->product_name.'</td>';
	$paginationHtml.='<td>'.$row->product_number.'</td>';
	$paginationHtml.='<td>'.$price.'</td>'; 
	$paginationHtml.='<td>'.$row->amount.'</td>';
	$paginationHtml.='<td>'.$row->short_description.'</td>';
	$paginationHtml.='<td><form method="DELETE" action="'.base_url('itemList/delete/'.$row->id).'">';
	$paginationHtml.='<a class="btn btn-info" href="'.base_url('itemList/show/'.$row->id).'"> Mutasd</a>';
	$paginationHtml.='<a class="btn btn-primary" href="'.base_url('itemList/edit/'.$row->id).'"> Szerkesztés</a>';
	$paginationHtml.='<button type="submit" class="btn btn-danger"> Törlés</button></form></td>';
        $paginationHtml.='</tr>';
        }
        $paginationHtml.='</tr></tbody></table></div>'; 
        
        $jsonData = array(
                "html"	=> $paginationHtml,	
        );
        echo json_encode($jsonData); 
        return $query;
    }
    
    public function recordCount() {
        return $this->db->count_all("product_details");
    }
    
    public function insertItem()
    {    
        $data = array(
            'product_name' => $this->input->post('prodname'),
            'product_number' => $this->input->post('prodnum'),
            'description' => $this->input->post('description'),
            'short_description' => $this->input->post('short_description')
        );
        $this->db->insert('product_details', $data);
        $product_id =$this->db->insert_id();
        $stockdata = array(
            'idproduct' => $product_id,
            'amount' => $this->input->post('amount')
        );
        $typedata = array(
            'idproduct' => $product_id,
            'type' => $this->input->post('type_option'),
            'gender' => $this->input->post('gender'),
            'size' => implode(",",$this->input->post('type_size')),
            'color' => implode(",",$this->input->post('type_color'))
        );
        $pricedata = array(
            'idproduct' => $product_id,
            'vatprice' => $this->input->post('vatprice'),
        );
        if(!empty($this->input->post("future_price")) && !empty($this->input->post("date_from")) && !empty($this->input->post("date_to"))){
            $pricedata = array(
                'idproduct' => $product_id,
                'vatprice' => $this->input->post('vatprice'),
                'future_price' => $this->input->post('future_price'),
                'date_from' => $this->input->post('date_from'),
                'date_to' => $this->input->post('date_to'),
                'is_discount' => 1,
            );
        }
        $this->db->insert('stock', $stockdata);
        $this->db->insert('prop', $typedata);
        $this->db->insert('price', $pricedata);
        //return $product_id;
        $this->addImages($product_id);
    }
    
    public function updateItem($id) 
    {
        $this->addImages($id);
        $data=array(
            'product_name' => $this->input->post('prodname'),
            'product_number' => $this->input->post('prodnum'),
            'description'=> $this->input->post('description'),
            'short_description'=> $this->input->post('short_description')
        );
        $stockdata=array(
            'amount' => $this->input->post('amount')
        );
        $this->db->where('id',$id);
        $this->db->update('product_details',$data);
        $this->db->where('idproduct',$id);
        $this->db->update('stock',$stockdata);
        $typedata = array(
            'gender' => $this->input->post('gender'),
            'size' => implode(",",$this->input->post('type_size')),
            'color' => implode(",",$this->input->post('type_color'))
        );
        $this->db->where('idproduct',$id);
        $this->db->update('prop', $typedata);
        if(!empty($this->input->post("future_price")) && !empty($this->input->post("date_from")) && !empty($this->input->post("date_to"))){
            $pricedata = array(
                'vatprice' => $this->input->post('vatprice'),
                'future_price' => $this->input->post('future_price'),
                'date_from' => $this->input->post('date_from'),
                'date_to' => $this->input->post('date_to'),
                'is_discount' => 1,
            );
        }else{
            $pricedata = array(
                'vatprice' => $this->input->post('vatprice'),
                'future_price' => null,
                'date_from' => null,
                'date_to' => null,
                'is_discount' => 0,
            );
        }
        $this->db->where('idproduct',$id);
        $this->db->update('price', $pricedata);
    }

    public function findItem($id)
    {
        $this->addImages($id);
        $this->db->select('product_details.*,stock.amount,prop.*,price.*');
        $this->db->from('product_details');
        $this->db->join('stock', 'stock.idproduct = product_details.id', 'left');
        $this->db->join('prop', 'prop.idproduct = product_details.id', 'left');
        $this->db->join('price', 'price.idproduct = product_details.id', 'left');
        $this->db->where('product_details.id',$id);
        $query = $this->db->get()->row();
        $color_arr = explode (",", $query->color);
        $query->color = $color_arr;
        $size_arr = explode (",", $query->size);
        $query->size = $size_arr;
        return $query;
    }
    
    public function deleteItem($id)
    {
        $this->db->delete('stock', array('idproduct' => $id));
        $this->db->delete('images', array('idproduct' => $id));
        $this->db->delete('prop', array('idproduct' => $id));
        $this->db->delete('price', array('idproduct' => $id));
        return $this->db->delete('product_details', array('id' => $id));
    }
    
    public function deleteItemPicture($id)
    {
        $image_path = 'uploads/files/'; 
        $query_get_image = $this->db->get_where('images', array('idimage' => $id));
        foreach ($query_get_image->result() as $record)
        {
            $filename = $image_path . $record->file_name; 
            if (file_exists($filename))
            {
                unlink($filename);
            }
        }
        return $this->db->delete('images', array('idimage' => $id));
    }
    
    function addImages($product_id)
    {
        $errorUploadType = $statusMsg = ''; 
        if($this->input->post('fileSubmit')){ 
            if(!empty($_FILES['files']['name']) && count(array_filter($_FILES['files']['name'])) > 0){ 
                $filesCount = count($_FILES['files']['name']); 
                for($i = 0; $i < $filesCount; $i++){ 
                    $_FILES['file']['name']     = $_FILES['files']['name'][$i]; 
                    $_FILES['file']['type']     = $_FILES['files']['type'][$i]; 
                    $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i]; 
                    $_FILES['file']['error']     = $_FILES['files']['error'][$i]; 
                    $_FILES['file']['size']     = $_FILES['files']['size'][$i]; 

                    // File upload configuration 
                    $uploadPath = 'uploads/files/'; 
                    $config['upload_path'] = $uploadPath; 
                    $config['allowed_types'] = 'jpg|jpeg|png|gif'; 
                    //$config['max_size']    = '100'; 
                    //$config['max_width'] = '1024'; 
                    //$config['max_height'] = '768'; 

                    // Load and initialize upload library 
                    $this->load->library('upload', $config); 
                    $this->upload->initialize($config); 

                    // Upload file to server 
                    if($this->upload->do_upload('file')){ 
                        // Uploaded file data 
                        $fileData = $this->upload->data();
                        $uploadData[$i]['idproduct'] = $product_id;
                        $uploadData[$i]['file_name'] = $fileData['file_name']; 
                        $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s"); 
                    }else{  
                        $errorUploadType .= $_FILES['file']['name'].' | ';  
                    } 
                }
                $errorUploadType = !empty($errorUploadType)?'<br/>File Type Error: '.trim($errorUploadType, ' | '):''; 
                if(!empty($uploadData)){ 
                    // Insert files data into the database
                    $insert = $this->db->insert_batch('images',$uploadData);

                    $statusMsg = $insert?'Files uploaded successfully!'.$errorUploadType:'Some problem occurred, please try again.'; 
                }else{ 
                    $statusMsg = "Sorry, there was an error uploading your file.".$errorUploadType; 
                } 
            }else{ 
                $statusMsg = 'Please select image files to upload.'; 
            } 
        }
        $this->db->select('idimage,file_name,uploaded_on'); 
        $this->db->from('images'); 
        if($product_id){ 
            $this->db->where('idproduct',$product_id); 
            $query = $this->db->get(); 
            $result = $query->result_array(); 
        }else{ 
            $this->db->order_by('uploaded_on','desc'); 
            $query = $this->db->get(); 
            $result = $query->result_array(); 
        }
        return $result;
    }
    
    function salesOrders($id = null){
        if($id == null){
        $query = $this->db->get('orders');
        return $query->result();
        }else{
            $this->db->where('idorder',$id);
            $query = $this->db->get('orders');
            return $query->result();
        }
    }
    
    function orderItems($id){
        $this->db->where('idorder',$id);
        $query = $this->db->get('order_items');
        return $query->result();
    }
}