<?php


class WsHomeModel extends CI_Model{
    
    public function __construct() {
        parent::__construct(); 
        $this->load->database();
        $this->load->helper('url', 'form');
        $this->load->library('cart');
    }
    
    public function getMainpageItems($limit,$type,$discounts,$notid,$filterarray=null){
        $this->db->select('product_details.*,stock.amount,price.*');
        $this->db->from('product_details');
        $this->db->join('stock', 'stock.idproduct = product_details.id', 'left');
        $this->db->join('price', 'price.idproduct = product_details.id', 'left');
        $this->db->join('prop', 'prop.idproduct = product_details.id', 'left');
        if($notid > 0){
            $this->db->where('product_details.id !=',$notid);
        }
        if(!$type == ""){
            $this->db->where('prop.type',$type);
        }
        if($filterarray['wgender'] != null){
            $this->db->where('prop.gender',$filterarray['wgender']);
        }
        if($discounts == 1){
            $date_now = date("Y-m-d"); 
            $this->db->where('price.date_from <=',$date_now);
            $this->db->where('price.date_to >=',$date_now);
        }
        if($filterarray['gender']!= null && $filterarray['gender'] != ''){
            $this->db->where('prop.gender', $filterarray['gender']);
        }
        if($filterarray['type'] != null && $filterarray['type'] != ''){
            $type_item = explode(",", $filterarray['type']);
            $this->db->where_in('prop.type', $type_item);   
        }
        if($filterarray['size'] != null && $filterarray['size'] != ''){
            $sizes = explode(",", $filterarray['size']);
            foreach($sizes as $sizee) {
                $this->db->like('prop.size', $sizee);
//                $this->db->where("FIND_IN_SET( '$sizee' , prop.size) "); 
            }
        }
        if($filterarray['color']!= null && $filterarray['color'] != ''){
            $colors = explode(",", $filterarray['color']);
            foreach($colors as $colorss) {
                $this->db->like('prop.color', $colorss);
            }
        }  
        if($filterarray['order_type'] != null && $filterarray['order_type'] != ''){
            if($filterarray['order_type'] == 'name_asc'){
                $this->db->order_by("product_details.product_name", "asc");
            }
            if($filterarray['order_type'] == 'name_desc'){
                $this->db->order_by("product_details.product_name", "desc");
            }
            if($filterarray['order_type'] == 'price_asc'){
                $this->db->order_by("price.vatprice", "asc");
            }
            if($filterarray['order_type'] == 'price_desc'){
                $this->db->order_by("price.vatprice", "desc");
            }
        }
        $this->db->limit($limit,$filterarray['offset']);
        $query = $this->db->get();
        foreach($query->result() as $dat){
            $dat->img_array = array();
            $dat->img_array = $this->getFirstImageById($dat->id);
            $dat->product_url = $this->getUrlByName($dat->product_name);
        }
        return $query->result();
    }
    
    
    public function getFilteredProducts(){
        $size='';
        if($this->input->post('size') != null && $this->input->post('size') != ''){
            $size = $this->input->post('size');
        }
        $gender='';
        if($this->input->post('gender') != null && $this->input->post('gender') != ''){
            $gender = $this->input->post('gender');
        }
        $type='';
        if($this->input->post('type') != null && $this->input->post('type') != ''){
            $type = $this->input->post('type');
        }
        $order_type='';
        if($this->input->post('order_type') != null && $this->input->post('order_type') != ''){
            $order_type = $this->input->post('order_type');
        }
        $offset=0;
        if($this->input->post('offset') != null && $this->input->post('offset') != ''){
            $offset = $this->input->post('offset');
        }
        $filterarray = array(
            'size'=> $size,
            'color'=> $this->input->post('color'),
            'gender'=> $gender,
            'type'=> $type,
            'order_type'=> $order_type,
            'wgender'=> null,
            'offset'=> $offset
        );
        $query = $this->getMainpageItems(6,"",0,0,$filterarray);
        $paginationHtml = "";
        if (count($query) > 0) {
            foreach($query as $row){
                $date_now = date("Y-m-d");
                $paginationHtml .= '<div class="col-md-4 col-sm-6 col-xs-12">';
                $paginationHtml .= '<div class="single-grid"><div class="grid-img">';
                if(!empty($row->img_array)){ foreach($row->img_array as $file){
                    if($file['idproduct'] == $row->idproduct){
                        $paginationHtml .= '<img class="img-resposive" src="'.base_url('uploads/files/'.$file['file_name']).'" alt="image" />';
                    }
                }}
                if ($date_now >= $row->date_from && $date_now <= $row->date_to) {
                    $paginationHtml .= '<strong>%</strong>';
                }
                $paginationHtml .= '<strong class="new">New</strong>';
                $paginationHtml .= '<div class="grid-overlay">';
                $paginationHtml .= '<ul><li><i class="fas fa-star"></i></li>';
                $paginationHtml .= '<li><i class="far fa-heart"></i></li>';
                $paginationHtml .= '<li><a href="'.base_url('productView/'.$row->product_url).'"><i class="far fa-arrow-alt-circle-right"></i></a></li>';
                $paginationHtml .= '</ul></div></div>';
                $paginationHtml .= '<h3><div class="card_product_name">'.$row->product_name;
                $paginationHtml .= '</div><span class="price-span">';
                if ($date_now >= $row->date_from && $date_now <= $row->date_to) {
                    $paginationHtml .= '<span class="float-none">Eredeti ár: <span class="float-none crossed_price">';
                    $paginationHtml .= $row->vatprice.'</span></span><span class="float-none">'.$row->future_price.'</span>';
                }else{
                    $paginationHtml .= $row->vatprice;
                }
                $paginationHtml .= ' Ft</span></h3>';
                $paginationHtml .= '<p>'.$row->short_description.'<p></div></div>';
            }
        }else{
            $paginationHtml .= '<h6>A keresési feltételeknek nem felel meg egy termék sem!</h6>';
        }
        $jsonData = array(
            "html" => $paginationHtml,
            "item_number" => count($query)
        );
        echo json_encode($jsonData);
    }
    
    public function getFirstImageById($product_id){
        $this->db->select('*'); 
        $this->db->from('images'); 
        $this->db->where('idproduct',$product_id); 
        $this->db->limit(1);
        $query = $this->db->get(); 
        return $query->result_array(); 
    }
    
    public function getProductIdByName($product_name){
        $product_name = str_replace('-', ' ', urldecode($product_name));
        $product_name = str_replace('_', '-', $product_name);
        //https://stackoverflow.com/questions/10023776/accented-characters-and-mysql-searching
        $accented= array("Ö","ö","Ü","ü","ű","Ó","ó","O","o","Ú","ú","Á","á","U","u","É","é","Í","í","ő", "Ű", "Ő", "ä","Ä","ű","Ű","ő","Ő");
        $nonaccented=array("O","o","U","u","u","O","o","O","o","U","u","A","a","U","u","E","e","I","i","o", "U", "O", "a","A","u","u","o","o");
        $product_name = str_replace($accented,$nonaccented,$product_name);
        $this->db->from('product_details'); 
        $this->db->like('product_name',$product_name); 
        $this->db->limit(1);
        $query = $this->db->get()->row(); 
        return $query->id;
    }
    
    public function getUrlByName($product_name){
        setlocale(LC_ALL, "en_US.utf8");
        $product_name = str_replace('-', '_', $product_name);
        $product_name = str_replace(' ', '-', $product_name);
        $product_name = strtr(utf8_decode($product_name), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
        return $product_name;
    }

    
    public function saveProductReview(){
        $query = false;
        if(!empty($this->input->post("rev_raiting")) && !empty($this->input->post("user_id")) && !empty($this->input->post("idproduct"))){
            $review_data = array(
                'idproduct' => $this->input->post("idproduct"),
                'iduser' => $this->input->post("user_id"),
                'rev_number' => $this->input->post("rev_raiting"),
                'message' => $this->input->post('message')
            );
            $query = $this->db->insert('reviews', $review_data);
        }
        return $query;
    }
    
    public function getProductReviews($id)
    {
        $this->db->where('idproduct',$id);
        $query = $this->db->get("reviews");
        return $query->result();
    }
    
    public function addProductTocart()
    {
        $cart_data = array(
            'qty' => $this->input->post("amount"),
            'size' => $this->input->post("size"),
            'color' => $this->input->post("color"),
            'id' => $this->input->post("idproduct"),
            'name' => $this->input->post("name"),
            'price' => $this->input->post("price"),
            'user_id' => $this->input->post("user_id")
        );
        $query = $this->cart->insert($cart_data);
        echo count($this->cart->contents());
        return $query;
    }
    public function checkout()
    {
        $data = array();
        foreach($this->cart->contents() as $imgdata){
            array_push($data, ...$this->getFirstImageById($imgdata['id']));
        }
        return $data;
    }
    
    public function deleteCartItem()
    {
        if(!empty($this->input->post("rowid"))){
            $data = array('rowid' => $this->input->post("rowid"), 'qty' => 0);
            $this->cart->update($data);
        }
        $cart_img_data = $this->checkout();
        $cartHtml = "";
        $var= "'";
        foreach ($this->cart->contents() as $items){
        $cartHtml.='<div class="form-group">';
        if(isset($cart_img_data)){ foreach($cart_img_data as $file){
            if($items['id'] == $file['idproduct']) {
        $cartHtml.='<div class="col-sm-3 col-xs-3"><img class="img-responsive" src="'.base_url('uploads/files/'.$file['file_name']).'"></div>';
        }}}
        $cartHtml.='<div class="col-sm-6 col-xs-6">';
	$cartHtml.='<div class="col-xs-12">'.$items['name'].'</div>';
	$cartHtml.='<div class="col-xs-12"><small>Mennyiség: '.$items['qty'].'</small></div>';
	$cartHtml.='<div class="col-xs-12"><small>Szín: '.$items['color'].'</small></div>';
	$cartHtml.='<div class="col-xs-12"><small>Méret: '.$items['size'].'</small></div></div>';
	$cartHtml.='<div class="col-sm-3 col-xs-3 text-right">'; 
	$cartHtml.='<h6>'.$items['price'].'<span> Ft</span></h6></div>'; 
	$cartHtml.='<div class="col-sm-3 col-xs-3 text-right">';
	$cartHtml.='<a class="afix-1" id="delete_cart_item" onclick="deleteItem('.$var.$items["rowid"].$var.')" href="#">';
	$cartHtml.='<i class="fa fa-trash" aria-hidden="true"></i></a>';
	$cartHtml.='</div></div><div class="form-group"><hr /></div>';
        }
        $jsonData = array(
                "cart_items" => $cartHtml,	
                "cart_total" => $this->cart->total(),	
        );
        echo json_encode($jsonData); 
    }
    
    public function insertOrder(){
        if(!empty($this->input->post("Bname")) && !empty($this->input->post("Bzip_code")) && !empty($this->input->post("Bcity"))){
            if(!empty($this->input->post("Baddress")) && !empty($this->input->post("Btel")) && !empty($this->input->post("Bemail"))){
                $order_data = array(
                    'Bname' => $this->input->post('Bname'),
                    'Bzip_code' => $this->input->post('Bzip_code'),
                    'Bvatno' => $this->input->post('Bvatno'),
                    'Bcity' => $this->input->post('Bcity'),
                    'Baddress' => $this->input->post('Baddress'),
                    'Btel' => $this->input->post('Btel'),
                    'Bemail' => $this->input->post('Bemail'),
                    'sameadr' => $this->input->post('sameadr'),
                    'Czip_code' => $this->input->post('Czip_code'),
                    'Ccity' => $this->input->post('Ccity'),
                    'Caddress' => $this->input->post('Caddress'),
                    'Ctel' => $this->input->post('Ctel'),
                    'order_total' => $this->cart->total(),
                    'Caddress' => $this->input->post('Caddress')
                );
                $this->db->insert('orders', $order_data);
                $order_id =$this->db->insert_id();
                foreach($this->cart->contents() as $cartdata){
                    $order_items = array(
                        'idorder' => $order_id,
                        'product_name' => $cartdata['name'],
                        'size' => $cartdata['size'],
                        'color' => $cartdata['color'],
                        'amount' => $cartdata['qty'],
                        'price' => $cartdata['price']
                    );
                    $this->db->insert('order_items', $order_items);
                    
                    //készlet levonás
                    $this->db->select('stock.amount, stock.id as stock_id');
                    $this->db->from('product_details');
                    $this->db->join('stock','stock.idproduct=product_details.id','left');
                    $this->db->where('product_details.product_name', $cartdata['name']);
                    $result = $this->db->get()->row();
                    $amount = $result->amount - $cartdata['qty'];
                    $stock_id = $result->stock_id;
                    
                    $this->db->where('stock.id',$stock_id);
                    $this->db->update('stock', array('amount' => $amount));
                }
                //kosár törlése
                $this->cart->destroy();
                //email kiküldése
                $this->sendMail($order_id);
            }
        }
    }
    function orderStatus(){
        if(!empty($this->input->post("order_status")) && !empty($this->input->post("idorder"))){
        $data = array(
            'order_status' => $this->input->post('order_status')
        );
        $this->db->where("idorder",$this->input->post("idorder"));
        $this->db->update('orders',$data);
        }
    }
    
    function sendMail($order_id){
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.nethely.hu',
            'smtp_port' => 465,
            'smtp_user' => 'levelezes@ciwebshop.hu',
            'smtp_pass' => 'Szakdolgozat2021',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'wordwrap' => TRUE
        );
        $order_data = $this->db->where("idorder",$order_id)->get("orders")->row();
        $itemstable = $this->orderedItems($order_id);
        $message = "<strong> Tisztelt ".$order_data->Bname."</strong>!<br/><br/>Köszönjük, hogy nálunk vásárolt!<br/><br/>A rendelése elfogadásra került.<br/>";
        $message .= "<br/>Kollégáink hamarosan elküldik a részletes visszaigazoló levelünket. ";
        $message .= $itemstable;
        $message .= "<br/><br/>Ha eltérést tapasztalna a rendelt termékekre vonatkozóan, kérem keresse fel kollégáinkat.";
        $message .= "<br/><br/>Amennyiben 1 munkanapon belül nem érkezik meg az email címére kérjük, nézze meg a levelezőjében a spamek között is, és ha ott sem találja, akkor írjon nekünk!";
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('levelezes@ciwebshop.hu');
        $this->email->to($order_data->Bemail);
        $this->email->subject('Rendelés visszaigazoló email');
        $this->email->message($message);
        if($this->email->send())
        {
            echo 'Email Kiküldve.';
        }
        else{
            show_error($this->email->print_debugger());
        }
    }
    
    function orderedItems($idorder){
        $this->db->where('idorder',$idorder);
        $ordered_items = $this->db->get('order_items');
        $tetelek = '<h3>Rendelt tételek : </h3>';
        $tetelek .= '<table border="0" cellpadding="8" cellspacing="0" ><thead><tr>';
        $tetelek .= "<th><p><strong>Terméknév</strong></p></th>";
        $tetelek .= "<th><p><strong>Mennyiség</strong></p></th>";
        $tetelek .= "<th><p><strong>Méret</strong></p></th>";
        $tetelek .= "<th><p><strong>Szín</strong></p></th>";
        $tetelek .= "<th><p><strong>Ár</strong></p></th>";
        $tetelek .= "</tr></thead><tbody>";
        foreach($ordered_items->result() as $item){
            $tetelek .= "<tr><td>".$item->product_name."</td>";
            $tetelek .= "<td>".$item->amount."</td>";
            $tetelek .= "<td>".$item->size."</td>";
            $tetelek .= "<td>".$item->color."</td>";
            $tetelek .= "<td>".$item->price."</td></tr>";
        }
        $tetelek .= "</tbody></table>";
        return $tetelek;
    }
    
    function saveContact(){
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.nethely.hu',
            'smtp_port' => 465,
            'smtp_user' => 'levelezes@ciwebshop.hu',
            'smtp_pass' => 'Szakdolgozat2021',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'wordwrap' => TRUE
        );
        $message = "<strong> Kapcsolattartó-form email </strong>!<br/><br/>";
        $message .="<strong> Tárgy: </strong>".$this->input->post("subject")."<br/><br/>";
        $message .="<strong> Név: </strong>".$this->input->post("name")."<br/><br/>";
        $message .="<strong> Email: </strong>".$this->input->post("email")."<br/><br/>";
        $message .="<strong> Üzenet: </strong>".$this->input->post("message")."<br/><br/>";
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('levelezes@ciwebshop.hu');
        $this->email->to('levelezes@ciwebshop.hu');
        $this->email->subject('Kapcsolat-form email');
        $this->email->message($message);
        if($this->email->send())
        {
            echo 'Email Kiküldve.';
        }
        else{
            show_error($this->email->print_debugger());
        }
    }
    
}

