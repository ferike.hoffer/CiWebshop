<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['image'] = "Upload_files/index";
$route['itemList'] = "ItemList/index";
$route['salesOrders'] = "ItemList/salesOrders";
$route['home'] = 'home/index';
$route['itemList/show/(:num)'] = "itemList/show/$1";
$route['itemList/orderDetails/(:num)'] = "itemList/orderDetails/$1";
$route['itemListCreate']['post'] = "itemList/store";
$route['itemListUpdate/(:any)']['put'] = "itemList/update/$1";
$route['itemListDelete/(:any)']['delete'] = "itemList/delete/$1";
//kép törlés
$route['itemListDeletePic/(:num)'] = "itemList/deletepic/$1";
//user
$route['register'] = 'user/register';
$route['login'] = 'user/login';
$route['logout'] = 'user/logout';
//pages
$route['mainpage'] = "WsHome/mainpage";
$route['productList'] = "WsHome/productList";
$route['productView/(:any)'] = "WsHome/productView/$1";
$route['getFilteredItems'] = "ItemList/getItemListFiltered";
$route['getFilteredProducts'] = "WsHome/getFilteredProducts";
//használatlan pagination volt
$route['itemList/(:num)'] = 'itemList/index/$1';
//review mentés
$route['saveReview'] = "WsHome/saveReview";
//termék kosárhoz adás
$route['addToCart'] = "WsHome/addToCart";
//checkout 1. oldal
$route['checkout'] = "WsHome/checkout";
$route['deleteCartItem'] = "WsHome/deleteCartItem";
//checkout mentés
$route['saveCheckout'] = "WsHome/storeOrder";
$route['orderStatus'] = "WsHome/orderStatus";
//contactUs
$route['contactUs'] = "WsHome/contactUs";
//contact_form mentés
$route['saveContact'] = "WsHome/saveContact";