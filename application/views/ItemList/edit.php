<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" type="text/css" />
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet"> 
<?php  //var_dump($item);?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb flexrow">
            <div class="pull-left">
                <h2>Termék szerkesztés</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="<?php echo base_url('itemList');?>"> Vissza</a>
            </div>
        </div>
    </div>
    <form method="post" enctype="multipart/form-data" action="<?php echo base_url('itemList/update/'.$item->id);?>">
        <?php
        if ($this->session->flashdata('errors')){
            echo '<div class="alert alert-danger">';
            echo $this->session->flashdata('errors');
            echo "</div>";
        }?>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-lg4">
                <div class="form-group">
                    <strong>Termék név :</strong>
                    <input type="text" name="prodname" class="form-control" value="<?php echo $item->product_name;?>">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg4">
                <div class="form-group">
                    <strong>Cikkszám :</strong>
                    <input type="text" name="prodnum" class="form-control" value="<?php echo $item->product_number;?>">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg4">
                <div class="form-group">
                    <strong>Ár :</strong>
                    <input type="text" name="vatprice" type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" value="<?php echo $item->vatprice;?>">
                </div>
                <h4 id="future_p">Kattintson ide az akciós ár beállításához</h4>
                <strong style="display:inline-block;">Akciós ár, dátum törlése :</strong>
                <input type="checkbox" id="myCheck" onclick="deleteFuturePrice()" style="display: inline-block;">
                <p id="text" style="display:none">Akciós ár,dátum törölve!</p>
                <h4 id="future_p2" class="hidden">Mégsem állítok be akciós árat</h4>
                <div class="form-group hidden open">
                <p>Akciós ár kezdete: <input type="text" id="date_from" name="date_from" value="<?php echo $item->date_from;?>"></p>
                <p>Akciós ár vége: <input type="text" id="date_to" name="date_to" value="<?php echo $item->date_to;?>"></p>
                <strong>Akciós ár :</strong>
                <input type="text" id="future_pric" name="future_price" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" value="<?php echo $item->future_price;?>">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg4">
                <div class="form-group">
                    <br>
                    <strong>Mennyiség :</strong>
                    <input type="text" name="amount" type="number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" value="<?php echo $item->amount;?>">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg4">
                <div class="form-group">
                    <strong>Rövid leírás :</strong>
                    <textarea name="short_description" class="form-control" ><?php echo $item->short_description;?></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg4">
                <div class="form-group">
                    <strong>Hosszú leírás :</strong>
                    <textarea name="description" class="form-control" ><?php echo $item->description;?></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg4">
                <div class="form-group" style="display: flex;flex-direction: column;">
                    <strong>Férfi vagy Női :</strong>
                    <select class="form-control" name="gender">
                        <option value="férfi" <?php if($item->gender == 'férfi'){echo 'selected';} ?> >Férfi</option>
                        <option value="női" <?php if($item->gender == 'női'){echo 'selected';} ?> >Női</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg4">
                <div class="form-group" style="display: flex;flex-direction: column;">
                    <strong>Méret :</strong>
                    <select id="p1" multiple="multiple" multiple name="type_size[]">
                        <option value="S" <?php foreach($item->size as $size){ if($size == 'S'){echo 'selected';}}?>>S</option>
                        <option value="M" <?php foreach($item->size as $size){ if($size == 'M'){echo 'selected';}}?>>M</option>
                        <option value="L" <?php foreach($item->size as $size){ if($size == 'L'){echo 'selected';}}?>>L</option>
                        <option value="XL" <?php foreach($item->size as $size){ if($size == 'XL'){echo 'selected';}}?>>XL</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg4">
                <div class="form-group" style="display: flex;flex-direction: column;">
                    <strong>Szín :</strong>
                    <select id="p2" name="type_color[]" multiple multiple="multiple">
                        <option value="fekete" <?php foreach($item->color as $color){ if($color == 'fekete'){echo 'selected';}}?>>Fekete</option>
                        <option value="világoskék" <?php foreach($item->color as $color){ if($color == 'világoskék'){echo 'selected';}}?>>Világos Kék</option>
                        <option value="sötétkék" <?php foreach($item->color as $color){ if($color == 'sötétkék'){echo 'selected';}}?>>Sötét Kék</option>
                        <option value="szürke" <?php foreach($item->color as $color){ if($color == 'szürke'){echo 'selected';}}?>>Szürke</option>
                        <option value="barna" <?php foreach($item->color as $color){ if($color == 'barna'){echo 'selected';}}?>>Barna</option>
                        <option value="piros" <?php foreach($item->color as $color){ if($color == 'piros'){echo 'selected';}}?>>Piros</option>
                    </select>
                </div>
            </div>
            <?php echo !empty($statusMsg)?'<p class="status-msg">'.$statusMsg.'</p>':''; ?>
            <div class="col-xs-12 col-sm-6 col-lg4">
                <div class="form-group">
                    <label>Válasszon képek/képeket</label>
                    <input type="file" class="form-control" name="files[]" multiple/>
                </div>
            </div>    
            <div class="">
                <h3 class="text-center">Kép feltöltés/szerkesztés</h3>
                <ul class="gallery">
                    <?php if(!empty($files)){ foreach($files as $file){ ?>
                    <li class="item text-center">
                        <img style="width: 200px;height: 200px;" src="<?php echo base_url('uploads/files/'.$file['file_name']); ?>" >
                        <p>Feltöltve ekkor: <?php echo date("j M Y",strtotime($file['uploaded_on'])); ?></p>
                        <a class="btn btn-primary" href="<?php echo base_url('itemListDeletePic/'.$file['idimage']);?>">Kép törlése</a>
                    </li>
                    <?php } }else{ ?>
                    <p>Fájl nem található...</p>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-xs-12 text-center m-30 edit_submit">
                <button type="submit" name="fileSubmit" value="UPLOAD" class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function() {
      $('#p1').multiselect();
      $('.multiselect-container input[type="checkbox"]').each(function(index,input){
       $(input).after( "<span></span>" );;
      });
      $('#p2').multiselect();
      $('.multiselect-container input[type="checkbox"]').each(function(index,input){
       $(input).after( "<span></span>" );;
      });
        $(function() {  
            $( "#date_from" ).datepicker({  
               appendText:"(yy-mm-dd)",  
               dateFormat:"yy-mm-dd",  
               altField: "#datepick-2",  
               altFormat: "DD, d MM, yy"  
            });  
        }); 
        $(function() {  
            $( "#date_to" ).datepicker({  
               appendText:"(yy-mm-dd)",  
               dateFormat:"yy-mm-dd",  
               altField: "#datepick-2",  
               altFormat: "DD, d MM, yy"  
            });  
        });
        $('#future_p').click(function(){
            $('#future_p2').removeClass('hidden').addClass("d-block");
            $('#future_p').removeClass('d-block').addClass("hidden");
            $('.open').addClass("d-block").removeClass('hidden');
        });
        $('#future_p2').click(function(){
            $('#future_p').removeClass('hidden').addClass("d-block");
            $('#future_p2').removeClass('d-block').addClass("hidden");
            $('.open').addClass("hidden").removeClass('d-block');
        });
        
    });
    function deleteFuturePrice() {
      var checkBox = document.getElementById("myCheck");
      var text_value = document.getElementById("future_pric");
      var text = document.getElementById("text");

      if (checkBox.checked == true){
        text_value.value = null;
        text.style.display = "block";
      } else {
        text.style.display = "none";
      }
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>  