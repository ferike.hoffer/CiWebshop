<div class="container" style="margin-top: 50px;">
    <div class="col-xs-12 pull-right">
        <a class="btn btn-primary pull-right" href="<?php echo base_url('salesOrders');?>"> Vissza</a>
    </div>
    <?php foreach ($order_data as $item) { ?>    
    <div class="col-xs-12 col-sm-6 col-lg4">
        <div class="form-group">
            <strong>Rendelés azonosító :</strong>
            <?php echo $item->idorder; ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg4">
        <div class="form-group">
            <strong>Megrendelő neve :</strong>
            <?php echo $item->Bname; ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg4">
        <div class="form-group">
            <strong>Megrendelő adószáma :</strong>
            <?php echo $item->Bvatno; ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg4">
        <div class="form-group">
            <strong>Megrendelő címe :</strong>
            <?php echo $item->Bzip_code.' '.$item->Bcity.' '.$item->Baddress; ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg4">
        <div class="form-group">
            <strong>Megrendelő email :</strong>
            <?php echo $item->Bemail; ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg4">
        <div class="form-group">
            <strong>Megrendelő tel. :</strong>
            <?php echo $item->Btel; ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg4">
        <div class="form-group">
            <strong>Rendelés végösszeg. :</strong>
            <?php echo $item->order_total.' Ft'; ?>
        </div>
    </div>
    <div class="col-xs-12 text-center mtb-15">
        <strong>Szállítói adatok :</strong>
        <br/>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg4">
        <div class="form-group">
            <strong>Szállítási cím. :</strong>
            <?php echo $item->Czip_code.' '.$item->Ccity.' '.$item->Caddress; ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg4">
        <div class="form-group">
            <strong>Kapcsolattartói tel. :</strong>
            <?php echo $item->Ctel; ?>
        </div>
    </div>
    <div class="col-xs-12 text-center mtb-15">
        <strong>Megrendelés státusza :</strong>
        <br/>
        <select class="form-control col-xs-6" name="type_option" onchange="changeOrderStatus(value,<?php echo $item->idorder; ?>)">
            <option value="1" <?php if($item->order_status == 1){ ?> selected <?php } ?>>Beszerzésre vár</option>
            <option value="2" <?php if($item->order_status == 2){ ?> selected <?php } ?>>Szállítás alatt</option>
            <option value="3" <?php if($item->order_status == 3){ ?> selected <?php } ?>>Teljesítve</option>
        </select>
    </div>
    <?php } ?>
    <div id="table">
    <table class="table table-bordered">
      <thead>
          <tr>
              <th>Termék neve</th>
              <th>Méret</th>
              <th>Szín</th>
              <th>Mennyiség</th>
              <th>Ár</th>
          </tr>
      </thead>
      <tbody>
       <?php foreach ($order_items as $order_item) { ?>      
          <tr>
            <td><?php echo $order_item->product_name; ?></td>
            <td><?php echo $order_item->size; ?></td>
            <td><?php echo $order_item->color; ?></td>
            <td><?php echo $order_item->amount; ?></td>
            <td><?php echo $order_item->price; ?></td>
          </tr>
          <?php } ?>
      </tbody>
    </table>
    </div>
</div>
<script>
function changeOrderStatus(statusId,id) {
    $.ajax({
        type: 'POST',
        url: '<?php echo base_url('orderStatus')?>',
        data: {
            'order_status': statusId,
            'idorder': id
        },
        success: function (msg) {
        }
    });
}
</script>