<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" type="text/css" />
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet"> 
<div class="container">
<div class="row">
    <div class="col-lg-12 margin-tb flexrow">
        <div class="pull-left">
            <h2>Termék hozzáadás</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="<?php echo base_url('itemList');?>"> Vissza</a>
        </div>
    </div>
</div>
<form method="post" enctype="multipart/form-data" action="<?php echo base_url('itemListCreate');?>">
    <?php
    if ($this->session->flashdata('errors')){
        echo '<div class="alert alert-danger">';
        echo $this->session->flashdata('errors');
        echo "</div>";
    }?>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Termék név:</strong>
                <input type="text" name="prodname" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Cikkszám:</strong>
                <input type="text" name="prodnum" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Ár :</strong>
                <input type="text" name="vatprice" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control">
            </div>
            <h4 id="future_p">Kattintson ide az akciós ár beállításához</h4>
            <strong style="display:inline-block;">Akciós ár, dátum törlése :</strong>
            <input type="checkbox" id="myCheck" onclick="deleteFuturePrice()" style="display: inline-block;">
            <p id="text" style="display:none">Akciós ár,dátum törölve!</p>
            <h4 id="future_p2" class="hidden">Mégsem állítok be akciós árat</h4>
            <div class="form-group hidden open">
            <p>Akciós ár kezdete: <input type="text" id="date_from" name="date_from"></p>
            <p>Akciós ár vége: <input type="text" id="date_to" name="date_to"></p>
            <strong>Akciós ár :</strong>
            <input type="text" id="future_pric" name="future_price" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <br>
                <strong>Mennyiség :</strong>
                <input type="text" name="amount" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Rövid leírás:</strong>
                <textarea name="short_description" class="form-control"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Hosszú leírás:</strong>
                <textarea name="description" class="form-control"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group" >
                <label for="genderid">Férfi vagy női :</label>
                <select class="form-control" name="gender" id="genderid">
                  <option value="férfi">Férfi</option>
                  <option value="női">Női</option>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group" >
                <label for="exampleFormControlSelect1">Típus :</label>
                <select class="form-control" name="type_option" id="exampleFormControlSelect1">
                  <option value="farmer">Farmer</option>
                  <option value="kabát">Kabát</option>
                  <option value="pulóver">Pulóver</option>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group" style="display: flex;flex-direction: column;">
                <strong>Méret :</strong>
                <select id="p1" multiple="multiple" multiple name="type_size[]">
<!--                        <option disabled selected value hidden style="display:none;">--Please select a value</option>-->
                    <option value="S">S</option>
                    <option value="M">M</option>
                    <option value="L">L</option>
                    <option value="XL">XL</option>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group" style="display: flex;flex-direction: column;">
                <strong>Szín :</strong>
                <select id="p2" name="type_color[]" multiple multiple="multiple">
                    
                    <option value="fekete">Fekete</option>
                    <option value="világoskék">Világos kék</option>
                    <option value="sötétkék">Sötét kék</option>
                    <option value="szürke">Szürke</option>
                    <option value="barna">Barna</option>
                    <option value="piros">Piros</option>
                </select>
            </div>
        </div>
        <?php echo !empty($statusMsg)?'<p class="status-msg">'.$statusMsg.'</p>':''; ?>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <label>Válasszon képet/képeket</label>
                <input type="file" class="form-control" name="files[]" multiple/>
            </div>
        </div>
        <div class="col-xs-12 text-center m-30 edit_submit">
            <button type="submit" name="fileSubmit" value="UPLOAD" class="btn btn-primary">Rögzítés</button>
        </div>
    </div>
</form>
</div>
<script>
    $(document).ready(function() {
        $('#p1').multiselect();
        $('.multiselect-container input[type="checkbox"]').each(function(index,input){
         $(input).after( "<span></span>" );;
        });
        $('#p2').multiselect();
        $('.multiselect-container input[type="checkbox"]').each(function(index,input){
         $(input).after( "<span></span>" );;
        });
        $(function() {  
            $( "#date_from" ).datepicker({  
               appendText:"(yy-mm-dd)",  
               dateFormat:"yy-mm-dd",  
               altField: "#datepick-2",  
               altFormat: "DD, d MM, yy"  
            });  
        }); 
        $(function() {  
            $( "#date_to" ).datepicker({  
               appendText:"(yy-mm-dd)",  
               dateFormat:"yy-mm-dd",  
               altField: "#datepick-2",  
               altFormat: "DD, d MM, yy"  
            });  
        });
        const input = document.querySelector("#future_pric");
        $('#future_p').click(function(){
            $('#future_p2').removeClass('hidden').addClass("d-block");
            $('#future_p').removeClass('d-block').addClass("hidden");
            $('.open').addClass("d-block").removeClass('hidden');
        });
        $('#future_p2').click(function(){
            input.value = null;
            $('#future_p').removeClass('hidden').addClass("d-block");
            $('#future_p2').removeClass('d-block').addClass("hidden");
            $('.open').addClass("hidden").removeClass('d-block');
        });
    });
    function deleteFuturePrice() {
      var checkBox = document.getElementById("myCheck");
      var text_value = document.getElementById("future_pric");
      var text = document.getElementById("text");

      if (checkBox.checked == true){
        text_value.value = null;
        text.style.display = "block";
      } else {
        text.style.display = "none";
      }
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>