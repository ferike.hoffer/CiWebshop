<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb flexrow">
            <div class="pull-left">
                <h2>Termék mutatása</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="<?php echo base_url('itemList');?>"> Vissza</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Termék név :</strong>
                <?php echo $item->product_name; ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Cikkszám :</strong>
                <?php echo $item->product_number; ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>ár :</strong>
                <?php echo $item->vatprice; ?>
            </div>
            <div class="form-group" style="display: flex;flex-direction: column;">
                <strong>Akciós ár kezdete :</strong> 
                <?php echo $item->date_from;?>
                <strong>Akciós ár vége : </strong>
                <?php echo $item->date_to;?>
                <strong>Akciós ár :</strong>
                <?php echo $item->future_price;?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Mennyiség :</strong>
                <?php echo $item->amount; ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Rövid leírás :</strong>
                <?php echo $item->short_description; ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Hosszú leírás :</strong>
                <?php echo $item->description; ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Férfi vagy női :</strong>
                <?php echo $item->gender; ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Típus :</strong>
                <?php echo $item->type; ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Méret :</strong>
                <?php foreach($item->size as $size){ echo $size.','; }?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg4">
            <div class="form-group">
                <strong>Szín :</strong>
                <?php foreach($item->color as $color){ echo $color.','; }?>
            </div>
        </div>
<!--        <div class="">
            <h3>Uploaded Files/Images</h3>
            <ul class="gallery">
                <?php //if(!empty($files)){ foreach($files as $file){ ?>
                <li class="item">
                    <img style="width: 200px;height: 200px;" src="<?php //echo base_url('uploads/files/'.$file['file_name']); ?>" >
                    <p>Uploaded On <?php //echo date("j M Y",strtotime($file['uploaded_on'])); ?></p>
                </li>
                <?php //} }else{ ?>
                <p>File(s) not found...</p>
                <?php //} ?>
            </ul>
        </div>-->
    </div>
    <div class="container">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php $active2 = true;$i = 0; ?>
                <?php foreach($files as $file):?>
                  <li class="<?php echo ($active2 == true)?"active":"" ?>" data-target="#myCarousel" data-slide-to="<?php $i ?>"> 
                  </li>
                <?php $active = false;$i++; ?>
            <?php endforeach; ?>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <?php $active = true; ?>
                <?php foreach($files as $file):?>
                  <div class="item <?php echo ($active == true)?"active":"" ?>"> 
                    <img src="<?php echo base_url('uploads/files/'.$file['file_name']); ?>" class="d-block w-100" style="width:100%;" alt="...">
                  </div>
                <?php $active = false; ?>
            <?php endforeach; ?>
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="far fa-arrow-alt-circle-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="far fa-arrow-alt-circle-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
</div>
