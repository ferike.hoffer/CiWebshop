<div class="container">
    <?php if (isset($error)) : ?>
            <div class="col-md-12">
                    <div class="alert alert-danger" role="alert">
                            <?= $error ?>
                    </div>
            </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12 margin-tb flexrow">
            <div class="pull-left">
                <h2>Termék Karbantartó</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success additem" href="<?php echo base_url('itemList/create') ?>">Új termék hozzáadása</a>
            </div>
        </div>
    </div>
    <div class="dislay-flex">
        <div class="order-select">
            <select id="order_type" class="h-25">
                <option value="edited_desc">Legújabbak elől</option>
                <option value="name_asc">Név szerint növekvő</option>
                <option value="name_desc">Név szerint csökkenő</option>
                <option value="price_asc">Ár szerint növekvő</option>
                <option value="price_desc">Ár szerint csökkenő</option>
            </select>
        </div>
        <div class="order-select">
            <select id="limit" class="h-25">
                <option value="4">4</option>
                <option value="8">8</option>
                <option value="10">10</option>
            </select>
        </div>
        <input type="text" placeholder="Keresés" name = "search" id="listsearch" style="margin-left: auto;"/>
    </div>
    <div id="table">
    <table class="table table-bordered" id="myTable">
      <thead>
          <tr>
              <th>Terméknév</th>
              <th>Cikkszám</th>
              <th>Ár</th>
              <th>Mennyiség</th>
              <th>Rövid leírás</th>
              <th width="220px">Művelet</th>
          </tr>
      </thead>
      <tbody>
       <?php foreach ($data as $item) { ?>      
          <tr>
              <td><?php echo $item->product_name; ?></td>
              <td><?php echo $item->product_number; ?></td>
              <td><?php $date_now = date("Y-m-d"); 
              if ($date_now >= $item->date_from && $date_now <= $item->date_to) {
                    echo $item->future_price;
                }else{
                    echo $item->vatprice;
                }?></td>
              <td><?php echo $item->amount; ?></td>
              <td><?php echo $item->short_description; ?></td>
          <td>
            <form method="DELETE" action="<?php echo base_url('itemList/delete/'.$item->id);?>">
              <a class="btn btn-info" href="<?php echo base_url('itemList/show/'.$item->id) ?>"> Mutasd</a>
             <a class="btn btn-primary" href="<?php echo base_url('itemList/edit/'.$item->id) ?>"> Szerkesztés</a>
              <button type="submit" class="btn btn-danger"> Törlés</button>
            </form>
          </td>     
          </tr>
          <?php } ?>
      </tbody>
    </table>
    </div>
<!--    <p class="pagination"><?php //foreach($links as $link){echo $link;} ?></p>-->
</div>
  <div id="pagdiv" class="margin_bottom_20 padding_top_10 padding_bottom_10 border_top_1 margin_top_20 text-center">
    <input type="hidden" id="current_page" value="1">
    <span class="pagination_div">
        <ul class="pagination no_margin_top no_margin_bottom">
            <li><span onclick="change_page('back','back', <?php echo $all_data; ?>)" class="pointer">&laquo;</span></li>
            <?php
            for ($i = 1; $i < $all_data + 1; $i++)
            {
                if ($i == 1)
                {
                    ?>
                    <li class="active pointer pager_<?php echo $i; ?> all_page" onclick="change_page(<?php echo $i; ?>, <?php echo $i; ?>, <?php echo $all_data; ?>)"><span><?php echo $i; ?></span></li>
                    <?php
                }
                else
                {
                    ?>
                    <li class="pointer pager_<?php echo $i; ?> all_page"><span onclick="change_page(<?php echo $i; ?>, <?php echo $i; ?>, <?php echo $all_data; ?>)"><?php echo $i; ?></span></li>
                    <?php
                }
            }
            ?>
            <li><span onclick="change_page('next','next', <?php echo $all_data; ?>)" class="pointer">&raquo;</span></li>
        </ul>
    </span>
</div>
<script language="javascript" type="text/javascript">
    document.getElementById("pagdiv");
    $(document).on("change", "#limit", function () {
        change_page();
    });
    $(document).on("change", "#order_type", function () {
        change_page();
    });
    
    function change_page(active_page, next_offset, all){
        var current_page = parseInt(($('#current_page').val() == '') ? 1 : $('#current_page').val());
        var select_page = false;
        if(typeof next_offset === 'number'&& (next_offset % 1) === 0){
            next_offset = (next_offset * $('#limit').val()) - $('#limit').val();
            select_page = true;
        }
        if (next_offset == 'back' && current_page >= $('#limit').val() && current_page > 1) //volt && current_page > 1
        {
            next_offset = current_page - $('#limit').val(); // -1 volt
            select_page = true;
        }
        if (next_offset == 'next' && current_page < all)
        {
            next_offset = current_page * $('#limit').val(); //+ 1 volt
            select_page = true;
        }
        if (next_offset != 'back' && next_offset != 'next')
        {
            select_page = true;
        }
        if (select_page)
        {
        $.ajax({ 
          url:'<?php echo base_url('getFilteredItems')?>',
          data : {
            search : $('#listsearch').val().trim(),
            limit: $('#limit').val(),
            order_type: $('#order_type').val(),
            start : next_offset
          },
          method:'POST',
          dataType:'json',
          success:function(data){
            $('#table').html(data.html); 
            $('.all_page').removeClass('active');
            $('.pager_' + active_page).addClass('active');
            $('#current_page').val(active_page); //
            jQuery('.pagination').find('.all_page').each(function ()
            {
                var pagenum = Number($(this).attr('data-pagenum'));
                var current_page = Number($('#current_page').val());
                if (current_page == 1 && pagenum <= 5)
                {
                    jQuery(this).removeClass('hidden');
                } else if (current_page == all && pagenum >= all - 4)
                {
                    jQuery(this).removeClass('hidden');
                } else
                {
                    if (pagenum < current_page - 2 && pagenum != 1)
                    {
                        jQuery(this).addClass('hidden');
                    } else if (!(pagenum < current_page - 2) && pagenum <= current_page + 2 && pagenum != all)
                    {
                        jQuery(this).removeClass('hidden');
                    } //else if (pagenum != 1 && pagenum != all)
//                    {
//                        jQuery(this).addClass('hidden');
//                    }
                    if (all > 5 && all - current_page > 3)
                    {
                        jQuery('.next_etc').removeClass('hidden');
                    } else
                    {
                        jQuery('.next_etc').addClass('hidden');
                    }
                }
            });
          },
          error:function(data){
          }
        });
        }
    }
    $('#listsearch').keyup(function(){
        var search = $(this).val();
        if(search != '')
        {
            change_page();
        }else{
            change_page();
        }
    });
</script>