<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<div class="container" style="margin-top: 50px;">
    <div id="table">
    <table class="table table-bordered" id="myTable">
      <thead>
          <tr>
              <th>Rendelés azonosító</th>
              <th>Megrendelő neve</th>
              <th>Megrendelő címe</th>
              <th>Rendelés végösszeg</th>
              <th>Rendelés dátuma</th>
              <th>Rendelés státusza</th>
              <th></th>
          </tr>
      </thead>
      <tbody><?php //var_dump($items); ?>
       <?php foreach ($items as $item) { ?>      
          <tr>
            <?php //var_dump($item); ?>
            <td><?php echo $item->idorder; ?></td>
            <td><?php echo $item->Bname; ?></td>
            <?php if($item->Ccity != null && $item->Caddress != null && $item->Czip_code != null) { ?>
            <td><?php echo $item->Czip_code.' '.$item->Ccity.' '.$item->Caddress; ?></td>
            <?php }else{ ?>
            <td><?php echo $item->Bzip_code.' '.$item->Bcity.' '.$item->Baddress; ?></td>
            <?php } ?>
            <td><?php echo $item->order_total; ?></td>
            <td><?php echo $item->date_record; ?></td>
            <td>
                <select class="form-control" name="type_option" onchange="changeOrderStatus(value,<?php echo $item->idorder; ?>)">
                  <option value="1" <?php if($item->order_status == 1){ ?> selected <?php } ?>>Beszerzésre vár</option>
                  <option value="2" <?php if($item->order_status == 2){ ?> selected <?php } ?>>Szállítás alatt</option>
                  <option value="3" <?php if($item->order_status == 3){ ?> selected <?php } ?>>Teljesítve</option>
                </select>
            </td>
            <td>
            <a class="btn btn-info" href="<?php echo base_url('itemList/orderDetails/'.$item->idorder) ?>"> Mutasd</a>
            </td>   
          </tr>
          <?php } ?>
      </tbody>
    </table>
    </div>
</div>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js" defer></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
    
    $('#myTable').DataTable({
        "language": {
            "decimal":        "",
            "emptyTable":     "Nem található rendelés.",
            "info":           "Mutat _START_ től _END_ -rekordig . Összesen _TOTAL_ ",
            "infoEmpty":      "Mutat 0 a 0 ból . Összesen 0",
            "infoFiltered":   "(Szűrve _MAX_ ből)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "_MENU_ Mutatása",
            "loadingRecords": "Töltés...",
            "processing":     "Feldolgozás...",
            "search":         "Keresés:",
            "zeroRecords":    "Nincs találat.",
            "paginate": {
                "first":      "Első",
                "last":       "Utolsó",
                "next":       "Következő",
                "previous":   "Előző"
            }
        }
    });
});
function changeOrderStatus(statusId,id) {
    $.ajax({
        type: 'POST',
        url: '<?php echo base_url('orderStatus')?>',
        data: {
            'order_status': statusId,
            'idorder': id
        },
        success: function (msg) {
        }
    });
}
</script>