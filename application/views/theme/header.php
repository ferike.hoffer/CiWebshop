<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- seo -->

    <title>Codeigniter 3 webshop és SEO - ciwebshop.hu</title>
    <meta name="description" content="Webshop készítése CodeIgniter keretrendszerrel és optimalizálása. Szakdolgozati feladat, vásárlásra lehetőség nincs! (CiWebshop)"/> <!-- seo -->
    <link rel="shortcut icon" type="image/jpg" href="<?php echo base_url(); ?>images/favicon_bag.png"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!--======== All Stylesheet =========-->
        <script src="https://kit.fontawesome.com/d6c7bf5478.js" crossorigin="anonymous"></script>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-EQH4B5K81F"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'G-EQH4B5K81F');
        </script>
</head>
<body>
    	<header id="site-header">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav" style="width: 100%;">
                                                <a class="navbar-brand" href="<?= base_url('mainpage') ?>">Főoldal</a>
						<?php if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true) : ?>
							<li class="logout"><a href="<?= base_url('logout') ?>">Kijelentkezés</a></li>
						<?php else : ?>
							<li class="register"><a href="<?= base_url('register') ?>">Regisztráció</a></li>
							<li class="login"><a href="<?= base_url('login') ?>">Bejelentkezés</a></li>
						<?php endif; ?>
						<?php if (isset($_SESSION['is_admin']) && $_SESSION['is_admin'] === true) :?>
							<div class="btn-group mainpage">
                                                            <button type="button" class="btn btn-default dropdown-toggle paddings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                              Karbantartó <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                              <li><a href="<?= base_url('itemList') ?>">Termék karbantartó</a></li>
                                                              <li><a href="<?= base_url('salesOrders') ?>">Rendelések</a></li>
                                                            </ul>
                                                        </div>
                                                        <?php endif; ?>
                                                        <li class="mainpage"><a href="<?= base_url('mainpage') ?>">Főoldal</a></li>
                                                        <li class="mainpage"><a href="<?= base_url('productList') ?>">Termékeink</a></li>
                                                        <li class="mainpage"><a href="<?= base_url('contactUs') ?>">Kapcsolat</a></li>
                                                        <li class="cart"><a href="<?= base_url('checkout') ?>"><i class="fas fa-shopping-cart"></i><span id="cart_value"><?php echo count($this->cart->contents()); ?></span></a></li>
                                                        
					</ul>
				</div><!-- .navbar-collapse -->
			</div><!-- .container-fluid -->
		</nav><!-- .navbar -->
	</header><!-- #site-header -->
        <main id="site-content" role="main">
<!--<div class="container">-->
