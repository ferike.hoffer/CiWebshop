<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
	</main><!-- #site-content -->

	<footer id="site-footer" class="footer-dark" role="contentinfo">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3 item">
                        <h3>Oldalaink</h3>
                        <ul>
                            <li><a href="<?= base_url('mainpage') ?>">Főoldal</a></li>
                            <li><a href="<?= base_url('productList') ?>">Termékeink</a></li>
                            <li><a href="<?= base_url('contactUs') ?>">Kapcsolat</a></li>
                            <li><a href="<?= base_url('checkout') ?>">Kosár</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3 item">
                        <h3>Kapcsolat</h3>
                        <ul class="flex-column">
                        <li class="">
                          <span class="footer-title">Lépjen velünk kapcsolatba!</span>
                        </li>
                        <li class="nav-item">
                          <span class="nav-link"><i class="fas fa-phone"></i> +36 45 80 80 808</span>
                        </li>
                        <li>
                          <span><i class="fas fa-envelope"></i>  levelezes@ciwebshop.hu</span>
                        </li>
                        <li>
                          <span><i class="fas fa-home"></i> Iszáki út 10, Kecskemét</span>
                        </li>
                      </ul>
                    </div>
                    <div class="col-md-3 item text">
                        <h3>Rólunk röviden</h3>
                        <p>Webshop készítése CodeIgniter keretrendszerrel és optimalizálása. Szakdolgozati projektfeladat 2021. Készítő: Hoffer Ferenc Joachim.</p>
                    </div>
                    <div class="col-md-3 item">
                        <ul class="list-inline social-buttons">
                            <li class="list-inline-item">
                              <a href="https://twitter.com">
                              <i class="fab fa-twitter"></i>
                            </a>
                            </li>
                            <li class="list-inline-item">
                              <a href="https://facebook.com">
                              <i class="fab fa-facebook-f"></i>
                            </a>
                            </li>
                            <li class="list-inline-item">
                              <a href="https://www.linkedin.com/">
                              <i class="fab fa-linkedin-in"></i>
                            </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <p class="copyright item">Szakdolgozat © 2021</p>
            </div>
	</footer><!-- #site-footer -->
<!--</div>-->
</body>
</html>