<!-- home -->
<section class="home">
    <div class="intro">
        <div class="container">
            <div class="row">
                    <div class="col-md-12 col-xs-12">
                            <h2>2021</h2>
                            <h1>CiWebshop Kollekciók</h1>
                            <small>Legjobb divat nőknek és férfiaknak azonnali készletről!</small>
                            <div><a href="<?php echo base_url('productList'); ?>" class="btn btn-default see-collection">Mutasd a termékeket</a></div>
                    </div>
            </div>    
        </div>    
    </div>    
</section>	

<!-- feature box -->    
<section class="feature-box">    
    <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12 mb-4">
                                <div class="f-s-b">
            <div class="well well-sm">
                <i class="fas fa-camera"></i>
                <h3>Legújabb trendek</h3>
            </div>
                                </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 mb-4">
                                <div class="f-s-b">
            <div class="well well-sm">
                <i class="fas fa-shopping-cart"></i>
                <h3>Kedvező árak</h3>
            </div>
                                </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 mb-4">
                                <div class="f-s-b">
            <div class="well well-sm">
                <i class="fab fa-angellist"></i>
                <h3>Különleges ajánlatok</h3>
            </div>
                                </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 mb-4">
                                <div class="f-s-b">
            <div class="well well-sm">
                <i class="fas fa-child"></i>
                <h3>Szezonális termékek</h3>
            </div>
                                </div>
                        </div>
                </div>    
        </div>    
</section>

<!-- Feature Product -->
<section class="feature-product">
    <div class="grid-product">
        <div class="container">
            <div class="row">
                <div class="section-title col-md-offset-3 col-md-6 col-xs-12 text-center">
                    <h2>Akciós termékeink</h2>
                    <small>Tekintse meg akciós ajánlatainkat</small>
                    <div class="section-border"><span class="dash"></span></div>
                </div>
            </div>	
            <div class="row">
                <?php foreach ($discounts as $item) { ?>   
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-grid">
                        <div class="grid-img">
                                <?php if(!empty($item->img_array)){ foreach($item->img_array as $file){?>
                                <?php if($file['idproduct'] == $item->idproduct){  ?>
                                <img class="img-resposive" src="<?php echo base_url('uploads/files/'.$file['file_name']);?>" alt="women" />
                                <?php }}} ?>
                                <strong>%</strong>
                                <div class="grid-overlay">
                                    <ul>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="far fa-heart"></i></li>
                                        <li><a href="<?php echo base_url('productView/'.$item->product_url) ?>"><i class="far fa-arrow-alt-circle-right"></i></a></li>
                                    </ul>
                                </div>
                        </div>	
                        <h3><div class="card_product_name"><?php echo $item->product_name; ?></div><span style="width: 100%"><span><?php $date_now = date("Y-m-d"); 
                        if ($date_now >= $item->date_from && $date_now <= $item->date_to) {
                        echo $item->future_price;
                        }else{echo $item->vatprice;}?> Ft</span><span>Eredeti ár: <span style="text-decoration: line-through; margin-right: 10px;"><?php echo $item->vatprice;?></span></span></span></h3>
                        <p><?php echo $item->short_description; ?></p>
                    </div>
                </div>
                <?php } ?>
            </div>    
        </div> 
    </div> 
</section>

<!-- News Trends -->
<section class="news-trends">
    <div class="grid-product">
        <div class="container">	
            <div class="row">
                <div class="section-title col-md-offset-3 col-md-6 col-xs-12 text-center">
                    <h2>Új pulóvereink</h2>
                    <small>Tekintse meg legújabb pulóvereinket</small>
                    <div class="section-border"><span class="dash"></span></div>
                </div>
            </div>
            <div class="row">
                <?php foreach ($data as $item) { ?>   
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-grid">
                        <div class="grid-img">
                                <?php if(!empty($item->img_array)){ foreach($item->img_array as $file){?>
                                <?php if($file['idproduct'] == $item->idproduct){  ?>
                                <img class="img-resposive" src="<?php echo base_url('uploads/files/'.$file['file_name']);?>" alt="women" />
                                <?php }}} ?>
                                <strong class="new">New</strong>
                                <div class="grid-overlay">
                                    <ul>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="far fa-heart"></i></li>
                                        <li><a href="<?php echo base_url('productView/'.$item->product_url) ?>"><i class="far fa-arrow-alt-circle-right"></i></a></li>
                                    </ul>
                                </div>
                        </div>	
                        <h3><div class="card_product_name"><?php echo $item->product_name; ?></div><span style="float: right;width: 100%;display: flex;justify-content: flex-end;"><?php $date_now = date("Y-m-d"); 
                        if ($date_now >= $item->date_from && $date_now <= $item->date_to) {?>
                        <span style="float: none;">Eredeti ár: <span style="text-decoration: line-through; margin-right: 10px;float: none;"><?php echo $item->vatprice;?></span></span>
                        <span style="float: none;"><?php echo $item->future_price; ?></span>
                        <?php } else{ echo $item->vatprice;} ?> Ft</span></h3>
                        <p><?php echo $item->short_description; ?></p>
                    </div>
                </div>
                <?php } ?>
            </div> 
        </div> 
    </div> 
</section> 

<!-- FAQ & Brands -->
<section class="faq-brands">
    <div class="container">	
    <div class="row">
        <div class="section-title col-md-offset-3 col-md-6 col-xs-12 text-center">
            <h2>Márkáink és GYIK</h2>
            <small>Vásároljon a legdivatosabb márkákból, hogy trendi maradjon!</small>
        <div class="section-border"><span class="dash"></span></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xs-12"> 
            <div id="brand" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#brand" data-slide-to="0" class="active"></li>
                    <li data-target="#brand" data-slide-to="1"></li>
                    <li data-target="#brand" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner" role="listbox" name="slider">
                    <div class="item active">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well well-lg">
                                    <img src="<?php echo base_url(); ?>images/brand5.png" alt="brand" />
                                </div>    
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well well-lg">
                                    <img src="<?php echo base_url(); ?>images/brand2.png" alt="brand" />
                                </div>    
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well well-lg">
                                    <img src="<?php echo base_url(); ?>images/brand3.png" alt="brand" />
                                </div>    
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well well-lg">
                                    <img src="<?php echo base_url(); ?>images/brand4.png" alt="brand" />
                                </div>    
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well well-lg">
                                    <img src="<?php echo base_url(); ?>images/brand5.png" alt="brand" />
                                </div>    
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well well-lg">
                                    <img src="<?php echo base_url(); ?>images/brand2.png" alt="brand" />
                                </div>    
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well well-lg">
                                    <img src="<?php echo base_url(); ?>images/brand3.png" alt="brand" />
                                </div>    
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well well-lg">
                                    <img src="<?php echo base_url(); ?>images/brand4.png" alt="brand" />
                                </div>    
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well well-lg">
                                    <img src="<?php echo base_url(); ?>images/brand5.png" alt="brand" />
                                </div>    
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well well-lg">
                                    <img src="<?php echo base_url(); ?>images/brand2.png" alt="brand" />
                                </div>    
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well well-lg">
                                    <img src="<?php echo base_url(); ?>images/brand3.png" alt="brand" />
                                </div>    
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="well well-lg">
                                    <img src="<?php echo base_url(); ?>images/brand4.png" alt="brand" />
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <div class="panel-group faq" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#1" aria-expanded="true"><i class="lnr lnr-inbox"></i>Divatinspiráció mindannyiunk számára</a></h4>
                    </div>
                    <div id="1" class="panel-collapse collapse in" role="tabpanel" for="headingOne">
                        <div class="panel-body">
                            <strong>Divatinspiráció mindannyiunk számára</strong>
                            <p>Divatáruink már több mint 25 éve inspirálják a hölgyeket ruhatáruk összeállításában. A rendszeresen megjelenő új kollekciók átértelmezik a legújabb trendeket és azokat a nők mindennapos szükségleteihez igazítják. Az S,M,L,XL méretű ruhák kiemelik a nőiség előnyeit és segítenek az öltözéken keresztül kifejezni az egyéniségüket.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#2" aria-expanded="false"><i class="lnr lnr-inbox"></i>Ruhák minden alkalomra</a></h4>
                    </div>
                    <div id="2" class="panel-collapse collapse" role="tabpanel" for="headingTwo">
                        <div class="panel-body">
                            <strong>Ruhák minden alkalomra</strong>
                            <p>Az elegáns estélyi ruháktól kezdve a hétköznapi (casual) és a sportos városi stílusig minden fellelhető. Elegáns esküvői ruhák, estélyi kreációk, szilveszteri öltözékek, nyári ruhák – ez csak néhány a legnépszerűbbek közül, melyek elnyerték a bonprix vásárlóinak tetszését.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#3" aria-expanded="false"><i class="lnr lnr-inbox"></i>Ügyféltámogatás és elégedettség</a></h4>
                    </div>
                    <div id="3" class="panel-collapse collapse" role="tabpanel" for="headingThree">
                        <div class="panel-body">
                            <strong>Ügyféltámogát és elégedettség</strong>
                            <p>Ügyfeleink körében nagy népszerűségnek örvendenek az XXL-es ruhák is. A méretek széles választékának köszönhetően minden hölgy megtalálhatja a számára megfelelő fazont és szabást, amelyben magabiztosnak és vonzónak érzi magát. A Ciwebshopnál mindenki, aki értékeli a kényelmes vásárlást, minden méretben stílusos fazonokkal találkozik, elérhető áron.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingfour">
                        <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#4" aria-expanded="false"><i class="lnr lnr-inbox"></i>Divatos ruhák mindenkinek</a></h4>
                    </div>
                    <div id="4" class="panel-collapse collapse" role="tabpanel" for="headingfour">
                        <div class="panel-body">
                            <strong>Divatos ruhák mindenkinek</strong>
                            <p>Divatárunk ugyanolyan sokszínű, mint vásárlóink valódi világa. Társa lehet a hölgyeknek a mindennapi, kihívásokkal teli úton és megtalálhatják a számukra és szeretteik számára az ideális öltözéket minden alkalomra. A női divat dominál, de ezen kívül kapható még férfi divat. Megtalálhatók tehát a férfi pulóverek és nadrágok ugyanúgy, mint a lányka ruhácskák vagy kabátok.</p>
                        </div>
                    </div>
                </div>
            </div><!--collapse-->
            </div>
        </div>    
    </div>
</section>