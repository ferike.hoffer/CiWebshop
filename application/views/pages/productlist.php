<!-- Product -->
<section class="product-list">
        <div class="grid-product">
                <div class="container">	
                        <div class="row">
        <!-- left sidebar -->
                                <div class="col-md-3">
                                        <div class="sidebar">
                <!-- category widget -->
                <div class="panel widget panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Ruhanem</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="category-list gender">
                            <li class="gender-variable-item" value="férfi"><i class="fas fa-male side-icon pull-left"></i> Férfi<i class="fa fa-angle-right pull-right"></i></li>
                            <li class="gender-variable-item" value="női"><i class="fas fa-female side-icon pull-left"></i> Női<i class="fa fa-angle-right pull-right"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="panel widget panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Ruha fajta</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="category-list type">
                            <li class="type-variable-item" value="farmer"> Farmer<i class="fa fa-angle-right pull-right"></i></li>
                            <li class="type-variable-item" value="pulóver"> Pulóver<i class="fa fa-angle-right pull-right"></i></li>
                            <li class="type-variable-item" value="kabát"> Kabát<i class="fa fa-angle-right pull-right"></i></li>
                        </ul> 
                    </div>
                </div>
                <!-- Size widget -->
                <div class="panel widget panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Méretek</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="list-inline size">
                            <li class="size-variable-item" value="S">S</li>
                            <li class="size-variable-item" value="M">M</li>
                            <li class="size-variable-item" value="L">L</li>
                            <li class="size-variable-item" value="XL">XL</li>
                        </ul>   
                    </div>
                </div>
                <!-- Manufatues widget -->
                <div class="panel manufatue widget panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Színek</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="category-list color">
                            <li class="color-variable-item" name="fekete" value="fekete"><i class="side-icon side-icon-black pull-left"></i> Fekete<i class="fa fa-angle-right pull-right"></i></li>
                            <li class="color-variable-item" name="világoskék" value="világoskék"><i class="side-icon side-icon-light-blue pull-left"></i> Világos kék<i class="fa fa-angle-right pull-right"></i></li>
                            <li class="color-variable-item" name="sötétkék" value="sötétkék"><i class="side-icon side-icon-blue pull-left"></i> Sötétkék<i class="fa fa-angle-right pull-right"></i></li>
                            <li class="color-variable-item" name="szürke" value="szürke"><i class="side-icon side-icon-grey pull-left"></i> Szürke<i class="fa fa-angle-right pull-right"></i></li>
                            <li class="color-variable-item" name="barna" value="barna"><i class="side-icon side-icon-brown pull-left"></i> Barna<i class="fa fa-angle-right pull-right"></i></li>
                            <li class="color-variable-item" name="piros" value="piros"><i class="side-icon side-icon-red pull-left"></i> Piros<i class="fa fa-angle-right pull-right"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- product grid -->
        <div class="col-md-9">
            <!-- subcategory -->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h3 class="women">Termékeink</h3>  
                    <h4 class="subcategory">Nálunk megtalálja az Önnek megfelelőt!</h4>
                    <div class="list-style rightfilter">
                        <strong id="item_number" style="display: flex;">6&nbsp;</strong><strong> Termék mutatása.  Összesen :&nbsp;</strong><strong> <?php echo $counted_items; ?></strong>
                        <div class="order-select" style="margin-bottom: 0px;margin-left: auto;"> 
                            <select id="order_type" class="h-25">
                                <option value="edited_desc">Legújabbak elől</option>
                                <option value="name_asc">Név szerint növekvő</option>
                                <option value="name_desc">Név szerint csökkenő</option>
                                <option value="price_asc">Ár szerint növekvő</option>
                                <option value="price_desc">Ár szerint csökkenő</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row m-30 dflex_row" id="tabledata">
                <?php foreach ($all_item as $item_data) { ?>   
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-grid">
                            <div class="grid-img">
                                    <?php if(!empty($item_data->img_array)){ foreach($item_data->img_array as $file){?>
                                    <?php if($file['idproduct'] == $item_data->idproduct){  ?>
                                    <img class="img-resposive" src="<?php echo base_url('uploads/files/'.$file['file_name']);?>" alt="image" />
                                    <?php }}} ?>
                                    <?php $date_now = date("Y-m-d"); if ($date_now >= $item_data->date_from && $date_now <= $item_data->date_to) { ?><strong>%</strong> <?php } ?>
                                    <strong class="new">New</strong>
                                    <div class="grid-overlay">
                                        <ul>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="far fa-heart"></i></li>
                                            <li><a href="<?php echo base_url('productView/'.$item_data->product_url) ?>"><i class="far fa-arrow-alt-circle-right"></i></a></li>
                                        </ul>
                                    </div>
                            </div>	
                            <h3><div class="card_product_name"><?php echo $item_data->product_name; ?></div><span class="price-span"><?php 
                                if ($date_now >= $item_data->date_from && $date_now <= $item_data->date_to) {?>
                                <span class="float-none">Eredeti ár: <span class="float-none crossed_price"><?php echo $item_data->vatprice;?></span></span>
                                <span class="float-none"><?php echo $item_data->future_price; ?></span>
                                <?php } else{ echo $item_data->vatprice;} ?> Ft</span></h3>
                             <p><?php echo $item_data->short_description; ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
                <div id="pagdiv" class="text-center">
                    <input type="hidden" id="current_page" value="1">
                    <span class="pagination_div">
                        <ul class="pagination">
                            <li><span onclick="change_page('back','back', <?php echo $all_data; ?>)" class="pointer">&laquo;</span></li>
                            <?php
                            for ($i = 1; $i < $all_data + 1; $i++)
                            {
                                if ($i == 1)
                                {
                                    ?>
                                    <li class="active pointer pager_<?php echo $i; ?> all_page" onclick="change_page(<?php echo $i; ?>, <?php echo $i; ?>, <?php echo $all_data; ?>)"><span><?php echo $i; ?></span></li>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <li class="pointer pager_<?php echo $i; ?> all_page"><span onclick="change_page(<?php echo $i; ?>, <?php echo $i; ?>, <?php echo $all_data; ?>)"><?php echo $i; ?></span></li>
                                    <?php
                                }
                            }
                            ?>
                            <li><span onclick="change_page('next','next', <?php echo $all_data; ?>)" class="pointer">&raquo;</span></li>
                        </ul>
                    </span>
                </div>
            </div>
            </div>
        </div>    
    </div> 
</section>
<script>
    $(document).on("change", "#order_type", function () {
        change_page();
    });
    function change_page(active_page, next_offset, all){
        var current_page = parseInt(($('#current_page').val() == '') ? 1 : $('#current_page').val());
        var select_page = false;
        if(typeof next_offset === 'number'&& (next_offset % 1) === 0){
            next_offset = (next_offset * <?php echo $per_page; ?>) - <?php echo $per_page; ?>;
            select_page = true;
        }
        if (next_offset == 'back' && current_page >= <?php echo $per_page; ?> && current_page > 1){
            next_offset = current_page - <?php echo $per_page; ?>; // -1 volt
            select_page = true;
        }
        if (next_offset == 'next' && current_page < all){
            next_offset = current_page * <?php echo $per_page; ?>; //+ 1 volt
            select_page = true;
        }
        if (next_offset != 'back' && next_offset != 'next'){
            select_page = true;
        }
        if (select_page){
        var gender_val = $('.gender .selectedli_gender').attr("value");
        var type_items = document.querySelectorAll( ".type .selectedli_tpye");
        var type_val = '';
        for( var n = 0; n < type_items.length; n++){
            type_val += type_items[n].attributes.value.value+',';
        }
        var color_items = document.querySelectorAll( ".color .selectedli_color");
        var color_val = '';
        for( var n = 0; n < color_items.length; n++){
            color_val += color_items[n].attributes.value.value+',';
        }
        var items = document.querySelectorAll( ".size .selectedli");
        var size_val = '';
        for( var n = 0; n < items.length; n++){
            size_val += items[n].innerHTML+',';
        }
            $.ajax({ 
              url:'<?php echo base_url('getFilteredProducts')?>',
              data : {
                gender: gender_val,
                size: size_val,
                type: type_val,
                color: color_val,
                order_type: $('#order_type').val(),
                offset : next_offset
              },
              method:'POST',
              dataType:'json',
              success:function(data){
                $('#tabledata').html(data.html); 
                $('#item_number').html(data.item_number+'&nbsp;'); 
                $('.all_page').removeClass('active');
                $('.pager_' + active_page).addClass('active');
                $('#current_page').val(active_page); //
                jQuery('.pagination').find('.all_page').each(function ()
                {
                    var pagenum = Number($(this).attr('data-pagenum'));
                    var current_page = Number($('#current_page').val());
                    if (current_page == 1 && pagenum <= 5){
                        jQuery(this).removeClass('hidden');
                    } else if (current_page == all && pagenum >= all - 4){
                        jQuery(this).removeClass('hidden');
                    } else{
                        if (pagenum < current_page - 2 && pagenum != 1){
                            jQuery(this).addClass('hidden');
                        } else if (!(pagenum < current_page - 2) && pagenum <= current_page + 2 && pagenum != all){
                            jQuery(this).removeClass('hidden');}
                        if (all > 5 && all - current_page > 3){
                            jQuery('.next_etc').removeClass('hidden');} 
                        else{
                            jQuery('.next_etc').addClass('hidden');}
                    }
                });
              },
              error:function(data){}
            });
            }
    }
    var $li_type = $('.type-variable-item').click(function() {
        if($(this).hasClass('selectedli_tpye')){
            $(this).removeClass('selectedli_tpye');
            change_page();
        }else{
            $(this).addClass('selectedli_tpye');
            change_page();
        }
    });
    var $li_size = $('.size-variable-item').click(function() {
        if($(this).hasClass('selectedli')){
            $(this).removeClass('selectedli');
            change_page();
        }else{
            $(this).addClass('selectedli');
            change_page();
        }
    });
    var $li_color = $('.color-variable-item').click(function() {
        if($(this).hasClass('selectedli_color')){
            $(this).removeClass('selectedli_color');
            change_page();
        }else{
            $(this).addClass('selectedli_color');
            change_page();
        }
    });
    var $li_gender = $('.gender-variable-item').click(function() {
        if(!$(this).hasClass('selectedli_gender')){
            $li_gender.removeClass('selectedli_gender');
        }
        if($(this).hasClass('selectedli_gender')){
            $(this).removeClass('selectedli_gender');
            change_page();
        }else{
            $(this).addClass('selectedli_gender');
            change_page();
        }
    });
</script>