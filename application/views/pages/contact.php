
<!-- page header -->    
<section class="page-header">
    <div class="page-header-overlay">
        <div class="container">
            <div class="row">
                <div class="section-title col-md-offset-3 col-md-6 text-center">
                    <h2>Kapcsolat</h2>
                    <p>Kérdésed adódott? Esetleg más ügyben szeretnéd felvenni velünk a kapcsolatot? Írj nekünk, és nemsokára jelentkezünk a válasszal.</p>
                </div>
            </div>
        </div>    
    </div>    
</section> 

 <!-- Contact -->
 <section class="contact" style="padding: 0px">
    <div class="container">
         <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <iframe class="g-map" src="https://maps.google.com/maps?q=kecskem%C3%A9t%20izs%C3%A1ki%20%C3%BAt%2010&t=&z=15&ie=UTF8&iwloc=&output=embed" width="100%" height="400" border="0" ></iframe>
            </div>
        </div>     
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="other-addr">
                    <h3>Lépjen kapcsolatba velünk!</h3>
                    <p>Ha van bármilyen kérdése, kérem ne hezitáljon, küldjön üzenetet. Válaszolunk 24 órán belül! </p>

                    <div class="location">
                        <i class="fas fa-map pull-left"><span></span></i>
                        <p> Iszáki út 10, Kecskemét</p>
                    </div>
                    <div class="email">
                        <i class="fas fa-envelope pull-left"><span></span></i>
                        <p> levelezes@ciwebshop.hu</p>
                    </div>
                    <div class="phone">
                        <i class="fas fa-phone pull-left"><span></span></i>
                        <p> +36 45 80 80 808</p>
                    </div>
                </div>
            </div>
            <!-- Contact form -->
            <div class="col-sm-8 col-sm-6 col-xs-12" style="padding-bottom: 25px;">
                <div class="row form">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" placeholder="Név">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" class="form-control" id="subject" placeholder="Tárgy">
                        </div>
                    </div>
                    <div class="col-sm-12 text-center">
                        <div class="form-group">
                            <textarea class="form-control" rows="6" placeholder="Üzenet" id="message"></textarea>
                        </div>
                        <button class="btn btn-default" type="submit" id="send_message"><i class="fa fa-envelope"></i> Üzenet küldése</button>
                        <h6 class="hidden" id="c_success">Sikeresen elküldte üzenetét számunkra, a lehető leghamarabb feldolgozzuk azt!</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</section>
<script>
    $(document).on('click','#send_message', function() {
        var message = document.getElementById("message").value;
        var name = document.getElementById("name").value;
        var email = document.getElementById("email").value;
        var subject = document.getElementById("subject").value;
        if(message != "" && name != "" && email != "" && subject != ""){
        $.ajax({
            type: "POST",
            url: '<?php echo base_url('saveContact'); ?>', 
            data: {
                'subject': subject,
                'name': name,
                'email': email,
                'message': message
            },
            success:function(data){
                $('#send_message').addClass("hidden");
                $('#c_success').removeClass("hidden").addClass("d-block");
            }
        });
        }
    });
</script>