<section class="single-product">
    <div class="container">
        <div class="row">	
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-product-img">
                      <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <?php $active2 = true;$i = 0; ?>
                                <?php foreach($files as $file):?>
                                  <li class="<?php echo ($active2 == true)?"active":"" ?>" data-target="#myCarousel" data-slide-to="<?php $i ?>"> 
                                  </li>
                                <?php $active = false;$i++; ?>
                            <?php endforeach; ?>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php $active = true; ?>
                                <?php foreach($files as $file):?>
                                  <div class="item <?php echo ($active == true)?"active":"" ?>"> 
                                    <img src="<?php echo base_url('uploads/files/'.$file['file_name']); ?>" class="d-block w-100" style="width:100%;" alt="...">
                                  </div>
                                <?php $active = false; ?>
                            <?php endforeach; ?>
                        </div>
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                          <span class="far fa-arrow-alt-circle-left"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                          <span class="far fa-arrow-alt-circle-right"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="single-product-detail">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default active"> <i class="fa fa-circle"></i>Raktáron elérhető</button>
                        <button type="button" class="btn btn-default"> <?php echo $item->amount; ?> - DB</button>
                    </div>
                    <h3 class="product_name_title"><?php echo $item->product_name; ?></h3>
                    <div class="list-inline">
                        <span>
                        <?php if(count($reviews) != 0){
                        $sumnumber = 0;
                        foreach($reviews as $review){ 
                        $sumnumber += $review->rev_number;}
                        $avarge = round($sumnumber/count($reviews));?>
                        <div class="justify-content-center">
                        <?php for($i=0;$i< (int)$avarge;$i++){ ?>
                            <label class="star-lbl" style="background: url(<?php echo base_url("images/star-label-yellow.png");?>);"></label>
                        <?php }?>
                        </div>
                        <?php } echo count($reviews); ?> - Értékelés</span>
                    </div>
                    <p>Ruhanem : <?php echo $item->gender; ?>&nbsp;&nbsp; Típusa : <?php echo $item->type ?></p>
                    <p><?php echo $item->short_description ?></p>
                    <table class="table table-responsive table-bordered">
                        <tr>
                            <td>Méret</td>
                            <td>
                                <ul class="list-inline size">
                                    <?php foreach($item->size as $size){ ?>
                                    <li class="size-variable-item"><?php echo $size ?></li>
                                    <?php } ?>
                                </ul>   
                            </td>
                        </tr>
                         <tr>
                            <td>Szín</td>
                            <td>
                                <ul class="list-inline color">
                                    <?php foreach($item->color as $color){ ?>
                                    <li class="color-variable-item"><?php echo $color ?></li>
                                    <?php } ?>
                                </ul>    
                            </td>
                        </tr>
                        <tr>
                            <td>Mennyiség</td>
                            <td>
                                <ul class="list-inline">
                                    <div class="input-counter form-group">
                                        <button type="button" class="btn btn-default amountbtn" onclick="minus()"><i class="fas fa-minus"></i></button>
                                        <input disabled="" name="EnteredQuantity" id="amount" type="text" value="1" class="qty-input cart-item-quantity form-control" autocomplete="off"">
                                        <button type="button" class="btn btn-default amountbtn" onclick="plus()"><i class="fas fa-plus"></i></button>
                                    </div>
                                </ul>
                            </td>
                        </tr>
                    </table>
                    <div class="price" style="height: 50px;"><h3><span style="float: left;"><?php $date_now = date("Y-m-d"); 
                    if ($date_now >= $item->date_from && $date_now <= $item->date_to) { ?>
                    <span style="float: none;">Eredeti ár: <span style="text-decoration: line-through; margin-right: 10px;float: none;"><?php echo $item->vatprice;?></span></span>
                    <span style="float: none;"><?php echo $item->future_price; ?></span>
                    <?php } else{ echo $item->vatprice;} ?> Ft</span></h3></div>
                    <div class="order-tag" style="display: inline-block;"><?php $date_now_ = date("Y-m-d"); 
                    if ($date_now_ >= $item->date_from && $date_now_ <= $item->date_to) { ?> Akciós még eddig: <?php echo $item->date_to; } ?></div>
                    <div class="add-cart" id="cart_to_add"><span class="btn btn-default" id="addtocart"><i class="far fa-arrow-alt-circle-right" style="width: auto;height: auto;"></i>Kosárba rakom</span></div>
                    <h4 class="hidden" id="sucess_cart">Sikeresen hozzáadta a kosárhoz</h4>
                </div>    
            </div>    
        </div>    
    </div> 
</section>

<!-- Product description -->    
<section class="single-description">
    <div cl class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
                <div class="description-tab">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab">Leírás</a></li>
                        <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Szállítási információk</a></li>
                        <li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Értékelés</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="tab1">
                            <h3><?php echo $item->short_description ?></h3>
                            <p><?php echo $item->description ?></p>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab3">
                            <h3>Szállítási lehetőségek és árak:</h3>
                            <p>E-mailben értesítünk arról, hogy a rendelésed elhagyta a raktárunkat és átadtuk a postának. Ebben az e-mailben linket is találsz, melynek segítségével nyomon követheted a csomagot.
                            <br/>A Magyar Posta futára kézbesíti a csomagot. Ha az adott időpontban a futár nem találja a címzettet otthon, akkor értesítést hagy vagy megpróbálja másnap ismét kézbesíteni a csomagot.
                            <br/>Az át nem vett megrendelés a postán 10 napig lesz megőrizve. Az át nem adott csomagot visszaküldik a raktárba.
                            <br/>Házhoz szállítás költsége minden rendelésnél + 1500 Ft.</p>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab4">
                            <h3>Eddigi értékelések: <?php echo count($reviews); ?>db értékelés</h3>
                            <div class="justify-content-center">
                                <?php foreach($reviews as $review){ ?>
                                    <div class="d-block showrev">
                                        <div class="rev_stars">
                                        <?php for($i=0;$i<$review->rev_number;$i++){ ?>
                                            <label class="star-lbl" style="background: url(<?php echo base_url("images/star-label-yellow.png");?>);"></label>
                                        <?php }?>
                                        </div>
                                        <div class="rev_comment"><?php echo $review->message; ?></div>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true){ ?>
                            <h3 class="hidden" id="rev_sucess">Köszönjük sikeresen rögzítettük az értékelését!</h3>
                            <h3 id="rev_title">Kérem értékelje termékünket!</h3>
                            <div class="justify-content-center" id="review_">
                                <ul class="review-star-list">
                                    <li>
                                        <label for="star-1" class="star-label"></label>
                                        <input data-val="true" data-val-number="The field Rating must be a number." id="star-1" name="AddProductReview.Rating" type="radio" value="1">
                                        <label for="star-2" class="star-label"></label>
                                        <input id="star-2" name="AddProductReview.Rating" type="radio" value="2">
                                        <label for="star-3" class="star-label"></label>
                                        <input id="star-3" name="AddProductReview.Rating" type="radio" value="3">
                                        <label for="star-4" class="star-label"></label>
                                        <input id="star-4" name="AddProductReview.Rating" type="radio" value="4">
                                        <label for="star-5" class="star-label"></label>
                                        <input checked="checked" id="star-5" name="AddProductReview.Rating" type="radio" value="5">
                                    </li>
                                </ul>
                                <div id="new-review">
                                    <form id="review_form_mobile" role="form">
                                        <div class="form-group">
                                            <textarea class="form-control" id="review_message" name="review_message" placeholder="Vélemény írása"></textarea>
                                        </div>
                                        <div class="review-bottom row">
                                            <div class="col-xs-12 col-md-6">
                                                <div class="review-star text-center" data-average="0" data-id="1"></div>
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <button type="button" class="btn btn-default" id="send_review">BEKÜLD</button>
                                            </div>
                                        </div>
                                    </form>
                                    <input type="hidden" id="rating_value_mobile" value="0">
                                </div>
                                <?php }else{ ?>
                                    <h3>Jelentkezzen be az értékeléshez!</h3>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</section>

<!-- Related Product -->    
<section>
    <div class="container">
        <?php if(!empty($related)){ ?>
        <div class="row">
            <div class="section-title col-md-offset-3 col-md-6 text-center">
                <h2>Hasonló termékek</h2>
                <small>Tekintse meg hasonló termékeinket is.</small>
                <div class="section-border"><span class="dash"></span></div>
            </div>
        </div>
        <?php } ?>

        <div class="row">
            <?php foreach ($related as $reldata) { ?>   
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-grid">
                        <div class="grid-img">
                                <?php if(!empty($reldata->img_array)){ foreach($reldata->img_array as $file){?>
                                <?php if($file['idproduct'] == $reldata->idproduct){  ?>
                                <img class="img-resposive" src="<?php echo base_url('uploads/files/'.$file['file_name']);?>" alt="women" />
                                <?php }}} ?>
                                <strong class="new">New</strong>
                                <div class="grid-overlay">
                                    <ul>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="far fa-heart"></i></li>
                                        <li><a href="<?php echo base_url('productView/'.$reldata->product_url) ?>"><i class="far fa-arrow-alt-circle-right"></i></a></li>
                                    </ul>
                                </div>
                        </div>	
                        <h3><div style="margin-bottom: 10px;"><?php echo $reldata->product_name; ?></div><span style="float: right;width: 100%;display: flex;justify-content: flex-end;"><?php $date_now = date("Y-m-d"); 
                        if ($date_now >= $reldata->date_from && $date_now <= $reldata->date_to) {?>
                        <span style="float: none;">Eredeti ár: <span style="text-decoration: line-through; margin-right: 10px;float: none;"><?php echo $item->vatprice;?></span></span>
                        <span style="float: none;"><?php echo $reldata->future_price; ?></span>
                        <?php } else{ echo $reldata->vatprice;} ?> Ft</span></h3>
                        <p><?php echo $reldata->short_description ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<script>
    var $li_size = $('.color-variable-item').click(function() {
        $li_size.removeClass('selectedli');
        $(this).addClass('selectedli');
    });
    var $li_color = $('.size-variable-item').click(function() {
        $li_color.removeClass('selectedli_color');
        $(this).addClass('selectedli_color');
    });
    var checkVal = "5";
    jQuery('.star-label').click(function () {
        checkVal = jQuery("#" + jQuery(this).attr("for"))[0].value;
        console.log(checkVal);
    });

    jQuery('.star-label').css('background', 'url(<?php echo base_url("images/star-label-yellow.png");?>)');
    jQuery('.star-label').click(function ()
    {
        jQuery('.star-label').css('background', 'url(<?php echo base_url("images/star-label-gray.png");?>)');
        var star = jQuery(this).attr('for');
        star = star.split('-');
        starCount = star[1];
        for (var i = 1; i <= starCount; i++)
        {
            jQuery('label[for=star-' + i + ']').css('background', 'url(<?php echo base_url("images/star-label-yellow.png");?>)');
        }
    });

    jQuery('.star-label').mouseenter(function ()
    {
        jQuery('.star-label').css('background', 'url(<?php echo base_url("images/star-label-gray.png");?>)');
        var star = jQuery(this).attr('for');
        star = star.split('-');
        starCount = star[1];
        for (var i = 1; i <= starCount; i++)
        {
            jQuery('label[for=star-' + i + ']').css('background', 'url(<?php echo base_url("images/star-label-yellow.png");?>)');
        }
    });

    jQuery('.review-rating ul').mouseleave(function ()
    {
        jQuery('.star-label').css('background', 'url(<?php echo base_url("images/star-label-gray.png");?>)');
        jQuery('.review-rating input[type="radio"]').each(function ()
        {
            if (jQuery(this).is(':checked'))
            {
                var id = jQuery(this).attr('id');
                id = id.split('-');
                starCount = id[1];
                for (var i = 1; i <= starCount; i++)
                {
                    jQuery('label[for=star-' + i + ']').css('background', 'url(<?php echo base_url("images/star-label-yellow.png");?>)');
                }
            }
        });
    });
    $(document).on('click','#send_review', function() {
        var message = document.getElementById("review_message").value;
        var id = <?php if (isset($_SESSION['username'])){
            echo $_SESSION['user_id']; }else{echo 0;} ?>;
        $.ajax({
            type: "POST",
            url: '<?php echo base_url('saveReview')?>', 
            data: {
                'rev_raiting': checkVal,
                'user_id': id,
                'idproduct': <?php echo $item->id ?>,
                'message': message
            },
            success:function(data){
                $('#review_').addClass("hidden");
                $('#rev_title').addClass("hidden");
                $('#rev_sucess').removeClass("hidden").addClass("d-block");
            }
        });
    });
    function plus(){
        if((Number($("#amount").val()) + 1) <= <?php echo $item->amount ?>){
            $("#amount").val(Number($("#amount").val()) + 1);
        }
    }

    function minus(){
        if((Number($("#amount").val()) - 1) > 0){
            $("#amount").val(Number($("#amount").val()) - 1);
        }
    }
    $(document).on('click','#addtocart', function() {
        if($('.size .selectedli_color').text() != '' && $('.color .selectedli').text() != ''){
            var size_val = $('.size .selectedli_color').text();
            var color_val = $('.color .selectedli').text();
            var id = <?php if (isset($_SESSION['username'])){
                echo $_SESSION['user_id']; }else{echo 0;} ?>;
            var price =  <?php if ($date_now >= $item->date_from && $date_now <= $item->date_to) {
                echo $item->future_price;
                }else{
                echo $item->vatprice;} ?>;
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('addToCart')?>',
                data: {
                    'amount': $("#amount").val(),
                    'size': size_val,
                    'color': color_val,
                    'idproduct': '<?php echo $item->id; ?>',
                    'name': '<?php echo $item->product_name; ?>',
                    'price': price,
                    'user_id': id
                },
                success: function (msg) {
                    $('#cart_to_add').addClass("hidden");
                    $('#sucess_cart').removeClass("hidden").addClass("d-block");
                    $('#cart_value').html(msg);
                }
            });
        }
    });
</script>