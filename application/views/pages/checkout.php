<div class="container wrapper">
    <div class="row cart-head">
        <div class="container">
            <br>
        </div><?php //php var_dump($this->cart->contents()); ?>
    <?php
    if ($this->session->flashdata('errors')){
        echo '<div class="alert alert-danger">';
        echo $this->session->flashdata('errors');
        echo "</div>";
    }?>
    <div class="row cart-body">
        <form class="form-horizontal" method="post" action="<?php echo base_url('saveCheckout');?>">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-push-6 col-sm-push-6">
            <!--REVIEW ORDER-->
            <div class="panel panel-info">
                <div class="ph">
                    Megrendelés összegzés
                </div>
                <div class="panel-body">
                    <div id="cart_items_view">
                    <?php foreach ($this->cart->contents() as $items){ ?>
                    <div class="form-group">
                        <div class="col-sm-3 col-xs-3">
                            <?php if(!empty($cart_img_data)){ foreach($cart_img_data as $file){
                            if($items['id'] == $file['idproduct']) {?>
                            <img class="img-responsive" src="<?php echo base_url('uploads/files/'.$file['file_name']);?>" />
                            <?php }}} ?>
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <div class="col-xs-12"><?php echo $items['name']; ?></div>
                            <div class="col-xs-12"><small>Mennyiség: <?php echo $items['qty']; ?></small></div>
                            <div class="col-xs-12"><small>Szín: <?php echo $items['color']; ?></small></div>
                            <div class="col-xs-12"><small>Méret: <?php echo $items['size']; ?></small></div>
                        </div>
                        <div class="col-sm-3 col-xs-3 text-right">
                            <h6><?php echo $items['price']; ?><span> Ft</span></h6>
                        </div>
                        <div class="col-sm-3 col-xs-3 text-right">
                            <a class="afix-1" id="delete_cart_item" onclick="deleteItem('<?php echo $items['rowid']; ?>')" href="#"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div><div class="form-group"><hr /></div>
                    <?php } ?>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <small>Rendelés összege: </small>
                            <div class="pull-right"><span id="cart_total"><?php echo $this->cart->total(); ?></span><span> Ft</span></div>
                        </div>
                        <div class="col-xs-12">
                            <small>Szállítási költség: </small>
                            <div class="pull-right"><span>1500</span><span> Ft</span></div>
                        </div>
                        <div class="col-xs-12"><br>
                            <strong>Teljes összeg: </strong>
                            <div class="pull-right"><span id="full_cart_total"><?php $cart = $this->cart->total();$shipping=1500; $total=$shipping+$cart;echo $total ?></span><span> Ft</span></div>
                        </div>
                    </div>
<!--                    <div class="form-group"><hr /></div>-->
                </div>
            </div>
            <!--REVIEW ORDER END-->
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-pull-6 col-sm-pull-6">
            <!--SHIPPING METHOD-->
            <div class="panel panel-info">
                <div class="ph">Számla adatok</div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            <h4>Rendelés adatok</h4>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-xs-12">
                            <label class="radio-inline" for="radio1A">
                            <input name="radio1" id="radio1" value="A" checked="checked" type="radio"/>Magánszemély</label>
                            <label class="radio-inline" for="radio1" style="margin-left: 0px;">
                            <input name="radio1" id="radio2" value="B" type="radio"/>Cég/vállalkozás</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-xs-12">
                            <strong class="hidden" id="comp_name">Cégnév/vállalkozás neve:</strong>
                            <strong id="pers_name">Név:</strong>
                            <input type="text" name="Bname" class="form-control" value="" />
                        </div>
                        <div class="span1"></div>
                        <div class="col-md-6 col-xs-12 hidden mt-10" id="vatno">
                            <strong>Adószám:</strong>
                            <input type="text" name="Bvatno" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6">
                            <strong>Irányítószám: </strong>
                            <div class="">
                                <input type="text" name="Bzip_code" class="form-control" value="" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"/>
                            </div>
                        </div>
                        <div class="col-sm-6">
                        <strong>Város:</strong>
                        <div><input type="text" name="Bcity" class="form-control" value="" /></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12"><strong>Utca/házszám: </strong></div>
                        <div class="col-md-12">
                            <input type="text" name="Baddress" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12"><strong>Telefonszám: </strong></div>
                        <div class="col-md-12">
                            <input type="text" name="Btel" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12"><strong>Email cím: </strong></div>
                        <div class="col-md-12">
                            <input type="text" name="Bemail" class="form-control" value="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <input type="checkbox" id="myCheck" onclick="CheckboxChecked()" checked="checked" name="sameadr"> A számlázási adatok megegyeznek a szallítási címmel
                        </div>
                    </div>
                    <div class="hidden" id="contact_form">
                        <div class="form-group">
                           <div class="col-md-12 text-center"><strong>Szállítási cím</strong></div>
                       </div>
                        <div class="form-group">
                           <div class="col-md-12"><strong>Irányítószám: </strong></div>
                           <div class="col-md-12"><input type="text" name="Czip_code" class="form-control" value="" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"/></div>
                       </div>
                       <div class="form-group">
                           <div class="col-md-12"><strong>Város :</strong></div>
                           <div class="col-md-12"><input type="text" name="Ccity" class="form-control" value="" /></div>
                       </div>
                       <div class="form-group">
                           <div class="col-md-12"><strong>Utca/házszám: </strong></div>
                           <div class="col-md-12"><input type="text" name="Caddress" class="form-control" value="" /></div>
                       </div>
                       <div class="form-group">
                           <div class="col-md-12"><strong>Kapcsolattartó telefonszám: </strong></div>
                           <div class="col-md-12"><input type="text" name="Ctel" class="form-control" value="" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"/></div>
                       </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <button type="submit" class="btn btn-primary btn-submit-fix">Rendelés leadása</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--SHIPPING METHOD END-->
        </div>
        </form>
    </div>
    <div class="row cart-footer">
    </div>
</div>
</div>
<script>
//    var sameadr = 1;
    function deleteItem(id){
        $.ajax({
            type: "POST",
            url: '<?php echo base_url('deleteCartItem')?>', 
            dataType:'json',
            data: {
                'rowid': id
            },
            success:function(data){
                $('#cart_items_view').html(data.cart_items);
                $('#cart_total').html(data.cart_total);
                $('#full_cart_total').html(data.cart_total+1500);
            }
        });
    }
    $('input[type=radio][name=radio1]').change(function() {
        if (this.value == 'A') {
            $('#pers_name').removeClass("hidden").addClass("d-block");
            $('#comp_name').removeClass("d-block").addClass("hidden");
            $('#vatno').removeClass("d-block").addClass("hidden");
        }
        else if (this.value == 'B') {
            $('#comp_name').removeClass("hidden").addClass("d-block");
            $('#pers_name').removeClass("d-block").addClass("hidden");
            $('#vatno').removeClass("hidden").addClass("d-block");
        }
    });
    function CheckboxChecked() {
        var checkBox = document.getElementById("myCheck");
        if (checkBox.checked == true){
            $('#contact_form').removeClass("d-block").addClass("hidden");
        } else {
            $('#contact_form').removeClass("hidden").addClass("d-block");
        }
    }
</script>