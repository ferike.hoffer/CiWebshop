<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WsHome extends CI_Controller {

    public $ItemList;
    public $WsHome;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('ItemListModel');
        $this->load->model('WsHomeModel');
        $this->ItemList = new ItemListModel;
        $this->WsHome = new WsHomeModel;
        $this->load->library('cart');//
        $this->load->library('form_validation');
        $this->output->set_header('Content-Type: text/html; charset=utf-8');
    }
    
    public function mainpage(){
        $data['data'] = $this->WsHome->getMainpageItems(4,"pulóver",0,0);
        $data['discounts'] = $this->WsHome->getMainpageItems(4,"",1,0);
        $this->load->view('theme/header');       
        $this->load->view('pages/mainpage',$data);
        $this->load->view('theme/footer');
    }
    
    public function productList(){
        //$data['data'] = $this->WsHome->getMainpageItems(4,"pulóver",0,0);
        $rows = $this->ItemList->recordCount();
        $per_page = 6;
        $data["counted_items"] = $rows;
        $data["per_page"] = $per_page;
        $data["all_data"] = $rows / $per_page;
        $data['all_item'] = $this->WsHome->getMainpageItems(6,"",0,0);
        $this->load->view('theme/header');       
        $this->load->view('pages/productlist',$data);
        $this->load->view('theme/footer');
    }
    
    public function getFilteredProducts(){
        $this->WsHome->getFilteredProducts();
    }


    public function productView($product_name){
        $id = $this->WsHome->getProductIdByName($product_name);
        $data['item'] = $this->ItemList->findItem($id);
        $data['files'] = $this->ItemList->addImages($id);
        $filterarray = array(
            'size'=> null,
            'color'=> null,
            'gender'=> null,
            'wgender'=> $data['item']->gender,
            'type'=> null,
            'order_type'=> null,
            'offset'=> 0
        );
        $data['related'] = $this->WsHome->getMainpageItems(4,$data['item']->type,0,$id,$filterarray);
        $data['reviews'] = $this->WsHome->getProductReviews($id);
        $this->load->view('theme/header');
        $this->load->view('pages/productview',$data);
        $this->load->view('theme/footer');
    }
    
    public function saveReview(){
        $this->WsHome->saveProductReview();
    }
    
    public function addToCart(){
        $this->WsHome->addProductTocart();
    }
    
    public function checkout(){
        $data['cart_img_data'] = $this->WsHome->checkout();
        $this->load->view('theme/header');
        $this->load->view('pages/checkout',$data);
        $this->load->view('theme/footer');
    }
    
    public function storeOrder()
    {
        $this->form_validation->set_message('required', '%s kitöltése kötelező!');
        $this->form_validation->set_rules('Bname', 'Név', 'required');
        $this->form_validation->set_rules('Bemail', 'Email cím', 'required|valid_email');
        $this->form_validation->set_rules('Bzip_code', 'Irányítószám', 'required');
        $this->form_validation->set_rules('Bcity', 'Város', 'required');
        $this->form_validation->set_rules('Baddress', 'Utca/házszám', 'required');
        $this->form_validation->set_rules('Btel', 'Telefonszám', 'required');
        if(count($this->cart->contents()) == 0){
            $this->form_validation->set_rules('carterror','Kosár','required');
        }
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url('checkout'));
        }else{
            $this->WsHome->insertOrder();
            redirect(base_url('ItemList'));
        }
    }
    
    public function orderStatus(){
        $this->WsHome->orderStatus();
    }
    
    public function deleteCartItem(){
        $this->WsHome->deleteCartItem();
    }
    
    public function contactUs(){
        $this->load->view('theme/header');
        $this->load->view('pages/contact');
        $this->load->view('theme/footer');
    }
    
    public function saveContact(){
        $this->WsHome->saveContact();
    }
}