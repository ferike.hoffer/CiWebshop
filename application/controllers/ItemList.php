<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ItemList extends CI_Controller {

    public $ItemList;
    
    public function __construct() {
      parent::__construct(); 
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model('ItemListModel');
        $this->ItemList = new ItemListModel;
        $this->load->library('pagination');
        $this->load->library('cart');
        if (!isset($_SESSION['is_admin']) ||  (isset($_SESSION['is_admin']) && $_SESSION['is_admin'] === false))
        {
            redirect(base_url());
        }   
    }

    public function index($page = 1)
    {
        $config = array();
        $config["base_url"] = base_url('itemList');
        $config["total_rows"] = $this->ItemList->recordCount();
        $config["per_page"] = 4;
//        $config['num_links'] = 4;
//        $config['cur_tag_open'] = '&nbsp;<a class="current">';
//        $config['cur_tag_close'] = '</a>';
//        $config['next_link'] = 'Következő';
//        $config['prev_link'] = 'Előző';
//        $this->pagination->initialize($config);
        $start = ($page - 1) * $config['per_page'];
        $str_links = $this->pagination->create_links();
        $data["links"] = explode('&nbsp;',$str_links );
        $data["all_data"] = $config["total_rows"] / $config["per_page"];
        $data["limit"] = $config["per_page"];
        $data["start"] = $start;
        $data['data'] = $this->ItemList->getItemList($config["per_page"], $start);
        $this->load->view('theme/header');       
        $this->load->view('ItemList/list',$data);
        $this->load->view('theme/footer');
    }
    public function getItemListFiltered()
    {
        $this->ItemList->getItemListPost();
    }

    public function show($id)
    {
        $item['item'] = $this->ItemList->findItem($id);
        $item['files'] = $this->ItemList->addImages($id);
        $this->load->view('theme/header');
        $this->load->view('ItemList/showlistitem',$item);
        $this->load->view('theme/footer');
    }

    public function create()
    {
        $this->load->view('theme/header');
        $this->load->view('ItemList/create');
        $this->load->view('theme/footer');
    }

    public function store()
    {
        $this->form_validation->set_message('required', '%s mező kitöltése kötelező!');
        $this->form_validation->set_rules('prodname', 'Név', 'required');
        $this->form_validation->set_rules('prodnum', 'Cikkszám', 'required');
        $this->form_validation->set_rules('vatprice', 'Ár', 'required');
        $this->form_validation->set_rules('amount', 'Mennyiség', 'required');
        $this->form_validation->set_rules('short_description', 'Rövid leírás', 'required');
        $this->form_validation->set_rules('description', 'Hosszú leírás', 'required');
        $this->form_validation->set_rules('gender', 'Ruhanem', 'required');
        $this->form_validation->set_rules('type_option', 'Típus', 'required');
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url('ItemList/create'));
        }else{
           $this->ItemList->insertItem();
           redirect(base_url('ItemList'));
        }
    }

    public function edit($id)
    {
       $data['item'] = $this->ItemList->findItem($id);
       $data['files'] = $this->ItemList->addImages($id);
       $this->load->view('theme/header');
       $this->load->view('ItemList/edit',$data);
       $this->load->view('theme/footer');
    }

    public function update($id)
    {
        //$this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url('ItemList/edit/'.$id));
        }else{ 
          $this->ItemList->updateItem($id);
          redirect(base_url('ItemList'));
        }
    }

    public function delete($id)
    {
        $this->ItemList->deleteItem($id);
        redirect(base_url('ItemList'));
    }
    
    public function deletepic($id)
    {
        $this->ItemList->deleteItemPicture($id);
        redirect(base_url('ItemList'));
    }
    
        public function salesOrders()
    {
        $data['items'] = $this->ItemList->salesOrders();
        $this->load->view('theme/header');
        $this->load->view('ItemList/sales_orders',$data);
        $this->load->view('theme/footer');
    }
    public function orderDetails($id)
    {
        $data['order_data'] = $this->ItemList->salesOrders($id);
        $data['order_items'] = $this->ItemList->orderItems($id);
        $this->load->view('theme/header');
        $this->load->view('ItemList/order_details', $data);
        $this->load->view('theme/footer');
    }
}